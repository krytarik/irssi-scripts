use strict;
use warnings;
use Irssi;

our $VERSION = '1.04';
our %IRSSI = (
    authors     => 'ilbelkyr, krytarik',
    contact     => 'ilbelkyr@shalture.org, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'account-notify',
    description => 'Display account identification status changes using CAP account-notify',
    license     => 'CC0 1.0 <https://creativecommons.org/publicdomain/zero/1.0/legalcode>',
    changed     => 'Thu Aug 22 03:56:04 CEST 2019',
);

my $cap_toggle = Irssi::Irc::Server->can('irc_server_cap_toggle');

sub on_account {
    my ( $server, $account, $nick, $address ) = @_;
    $account =~ s/^://;
    my @refnums;

    # Print the notification to all shared channels
    foreach my $channel ( $server->channels() ) {
	if ( $channel->nick_find($nick)
		&& ! check_ignore( $server, $nick, $address, $channel->{name} ) ) {
	    my $refnum = $channel->window()->{refnum};
	    unless ( grep { $_ == $refnum } @refnums ) {
		push( @refnums, $refnum );
		print_account( $channel, $nick, $account );
	    }
	}
    }

    # ...and the nick's query if any
    if ( ( my $query = $server->query_find($nick) )
	    && ! check_ignore( $server, $nick, $address, '' ) ) {
	my $refnum = $query->window()->{refnum};
	unless ( grep { $_ == $refnum } @refnums ) {
	    push( @refnums, $refnum );
	    print_account( $query, $nick, $account );
	}
    }
}

sub check_ignore {
    my ( $server, $nick, $address, $channel ) = @_;

    if ( $server->ignore_check( $nick, $address, $channel, '', MSGLEVEL_JOINS ) ) {
	return 1;
    }

    if ( my $smartfilter = Irssi::Script::smartfilter->can('smartfilter') ) {
	if ( $smartfilter->( $server, $channel, $nick, '', $address, 'status' ) ) {
	    return 1;
	}
    }
}

sub print_account {
    my ( $witem, $nick, $account ) = @_;

    if ( $account ne '*' ) {
	$witem->printformat( MSGLEVEL_JOINS, 'account_identify', $nick, $account );
    } else {
	$witem->printformat( MSGLEVEL_JOINS, 'account_logout', $nick );
    }
}

sub on_cap {
    my ( $server, $data ) = @_;

    if ( $data =~ m/^\S+ NAK :account-notify\s*$/ ) {
	Irssi::signal_stop();
    }
}

sub on_connected {
    my ( $server ) = @_;

    if ( $cap_toggle ) {
	$server->irc_server_cap_toggle( 'account-notify', 1 );
    } else {
	$server->command( 'quote cap req :account-notify' );
    }
}

Irssi::theme_register([
    account_identify => '{channick_hilight $0} has identified as {hilight $1}',
    account_logout   => '{channick $0} has logged out',
]);

Irssi::signal_add_first( 'event cap', 'on_cap' ) unless $cap_toggle;

Irssi::signal_add({
    'event account'   => 'on_account',
    'event connected' => 'on_connected',
});

foreach my $server ( Irssi::servers() ) {
    on_connected($server);
}
