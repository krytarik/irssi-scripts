use strict;
use warnings;
use Irssi;
use Encode;

our $VERSION = '1.8';
our %IRSSI = (
    authors     => 'Jakub Jankowski, Krytarik Raido',
    contact     => 'shasta@atn.pl, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Rainbow',
    description => 'Prints colored text. Rather simple than sophisticated.',
    license     => 'GNU GPLv2 or later',
);

# USAGE
# /rsay <text>
#  - same as /say, but colors the text
#
# /rme <text>
#  - same as /me, but colors the text
#
# /rtopic <text>
#  - same as /topic, but colors the text
#
# /rkick <nick> [reason]
#  - kicks nick from the current channel with colored reason
#
# /rknockout [time] <nick> [reason]
#  - knockouts nick from the current channel with colored reason for time
#
# /raddress [nick]: <text>
#  - addresses or not nick with colored text, without coloring the nick too

# SETTINGS
# /set rainbow_colors [colors]
#  - space-separated list of color codes
# /set rainbow_linear [on|off]
#  - linear or random coloring

# COLORS
#  0 = white
#  4 = light red
#  8 = yellow
#  9 = light green
# 11 = light cyan
# 12 = light blue
# 13 = light magenta

# CHANGES
# 25.01.2002 - Initial release (v1.0)
# 26.01.2002 - /rtopic added (v1.1)
# 29.01.2002 - /rsay works with dcc chats now (v1.2)
# 02.02.2002 - make_colors() doesn't assign any color to spaces (v1.3)
# 23.02.2002 - /rkick added
# 26.11.2014 - utf-8 support
# 01.12.2014 - /rknockout added (v1.6)
# 03.03.2017 - Slight overhaul, add color setting, add /raddress (v1.7)
# 10.04.2017 - Add option for linear coloring (v1.8)

sub make_colors {
	my ($text) = @_;
	Encode::_utf8_on($text);
	my ($textcol, $color, $last) = ("", -1, 255);

	my @colors = split(' ', Irssi::settings_get_str('rainbow_colors'));
	my $linear = Irssi::settings_get_bool('rainbow_linear');
	my $colcnt = scalar(@colors);

	for my $char (split(//, $text)) {
		if ($char ne " ") {
			unless ($linear) {
				until (($color = int(rand($colcnt))) != $last) {};
				$last = $color;
			} else {
				$color = 0 if ++$color == $colcnt;
			}
			$textcol .= sprintf("\003%02d%s", $colors[$color], $char);
		} else {
			$textcol .= $char;
		}
	}

	return $textcol;
}

sub rsay {
	my ($text, $server, $witem) = @_;
	return unless $witem;

	$witem->command("/msg * " . make_colors($text));
}

sub rme {
	my ($text, $server, $witem) = @_;
	return unless $witem;

	$witem->command("/me " . make_colors($text));
}

sub rtopic {
	my ($text, $server, $witem) = @_;
	return unless $witem;

	$witem->command("/topic " . make_colors($text));
}

sub rkick {
	my ($text, $server, $witem) = @_;
	return unless $witem;

	my ($nick, $reason) = split(' ', $text, 2);
	return unless $nick;
	$reason = "Irssi power!" unless $reason;
	$witem->command("/kick " . $nick . " " . make_colors($reason));
}

sub rknockout {
	my ($text, $server, $witem) = @_;
	return unless $witem;

	$text =~ /^\s*([0-9]+)?\s*(\S+)?\s*(.*?)\s*$/;
	my ($time, $nick, $reason) = ($1, $2, $3);

	return unless $nick;
	$time = 300 unless $time;
	$reason = "See you in " . $time . " seconds!" unless $reason;
	$witem->command("/knockout " . $time . " " . $nick . " " . make_colors($reason));
}

sub raddress {
	my ($args, $server, $witem) = @_;
	return unless $witem;

	$args =~ /^\s*(.*?)\s*:+\s*(.*?)\s*$/;
	my ($nicks, $text) = ($1, $2);

	return unless $text;
	if ($nicks) {
		$nicks =~ s/[\s,]+/, /g;
		$nicks .= ": ";
	}
	$witem->command("/msg * " . $nicks . make_colors($text));
}

Irssi::settings_add_str('rainbow', 'rainbow_colors', '0 4 8 9 11 12 13');
Irssi::settings_add_bool('rainbow', 'rainbow_linear', 0);

Irssi::command_bind({
	rsay      => 'rsay',
	rme       => 'rme',
	rtopic    => 'rtopic',
	rkick     => 'rkick',
	rknockout => 'rknockout',
	raddress  => 'raddress',
}, 'rainbow');
