# Settings:
#   window_hidden_level     - levels to hide/show with below commands
# Commands:
#   /window togglelevel     - hide/show levels in active window
#   /window togglelevel all - hide/show levels in all windows
#   /hide_levels            - hide levels in active window
#   /hide_levels_all        - hide levels in all windows
#   /show_levels            - show levels in active window
#   /show_levels_all        - show levels in all windows

use strict;
use warnings;
use Irssi v1.1;
use Irssi::TextUI;

our $VERSION = '0.7';
our %IRSSI = (
    authors     => 'Jari Matilainen, Krytarik Raido',
    contact     => 'vague!#irssi@freenode on irc, krytarik@tuxgarage.com',
    url         => 'https://git.launchpad.net/~krytarik/+git/irssi-scripts',
    name        => 'Toggle Winlevels',
    description => 'Toggles hidden levels on windows.',
    license     => 'GPLv2',
    changed     => '16.05.2020 02:13 CEST'
);

my $windows;
my $processing = 0;
my $hiddenbits = Irssi::level2bits('HIDDEN');

sub set_mode {
  my ($win, $mode) = @_;
  my ($lvlbits, $levels) = ($win->view->{hidden_level});
  $lvlbits = $windows->{$win->{refnum}}{levels} unless $lvlbits % $hiddenbits;
  $lvlbits = Irssi::settings_get_level('window_hidden_level') unless $lvlbits % $hiddenbits;
  $lvlbits = $win->view->{hidden_level} || $windows->{$win->{refnum}}{levels} unless $lvlbits;

  if($lvlbits) {
    $mode = $windows->{$win->{refnum}}{mode} ^ 1 unless defined $mode;
    update_status($win, $lvlbits, $mode);
    $levels = Irssi::bits2level($lvlbits);
    $levels =~ s/\w+/@{[$mode ? '+' : '-']}$&/g;
  } else {
    $lvlbits = Irssi::settings_get_level('window_default_hidelevel');
    update_status($win, $lvlbits);
    $levels = Irssi::bits2level($lvlbits);
    $levels =~ s/\w+/+$&/g;
  }

  if($levels) {
    $processing++;
    $win->command("^window hidelevel $levels");
  }
}

sub set_mode_single {
  my ($witem, $mode) = @_;
  if($witem) {
    set_mode($witem->window, $mode);
  } else {
    set_mode(Irssi::active_win, $mode);
  }
}

sub set_mode_all {
  my ($mode) = @_;
  for my $win (Irssi::windows) {
    set_mode($win, $mode);
  }
}

sub update_status {
  my ($win, $lvlbits, $mode) = @_;
  $lvlbits = $win->view->{hidden_level} unless defined $lvlbits;
  $mode = $lvlbits % $hiddenbits ? 1 : 0 unless defined $mode;
  $windows->{$win->{refnum}}{levels} = $lvlbits;
  $windows->{$win->{refnum}}{mode} = $mode;
}

Irssi::settings_add_level('lookandfeel', 'window_hidden_level', 'HIDDEN');

Irssi::signal_add_last(
  'window created' => sub {
    my ($win) = @_;
    update_status($win);
  }
);

Irssi::signal_add({
  'window destroyed' => sub {
    my ($win) = @_;
    delete $windows->{$win->{refnum}};
  },
  'window refnum changed' => sub {
    my ($win, $old_refnum) = @_;
    $windows->{$win->{refnum}} = delete $windows->{$old_refnum};
  }
});

Irssi::command_bind({
  'hide_levels' => sub {
    my ($data, $server, $witem) = @_;
    set_mode_single($witem, 1);
  },
  'hide_levels_all' => sub {
    set_mode_all(1);
  },
  'show_levels' => sub {
    my ($data, $server, $witem) = @_;
    set_mode_single($witem, 0);
  },
  'show_levels_all' => sub {
    set_mode_all(0);
  },
  'window togglelevel' => sub {
    my ($data, $server, $witem) = @_;
    my @args = split(' ', $data);
    unless(@args && $args[0] eq 'all') {
      set_mode_single($witem);
    } else {
      Irssi::command_runsub('window togglelevel', $data, $server, $witem);
    }
  },
  'window togglelevel all' => sub {
    set_mode_all();
  }
});

Irssi::command_bind_last(
  'window hidelevel' => sub {
    my ($args, $server, $witem) = @_;

    if($processing) {
      $processing--;
      return;
    }

    if($witem && $args) {
      update_status($witem->window);
    }
  }
);

for my $win (Irssi::windows) {
  update_status($win);
}
