use strict;
use warnings;
use Irssi;
use Irssi::TextUI;

our $VERSION = '1.23';
our %IRSSI = (
    authors     => 'David Leadbeater, Timo Sirainen, Georg Lukas, Krytarik Raido',
    contact     => 'dgl@dgl.cx, tss@iki.fi, georg@boerde.de, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'User Count',
    description => 'Adds a user count for a channel as a statusbar item',
    license     => 'GNU GPL v2 or later',
);

# Once you have loaded this script, add the user count with:
#   /statusbar window add usercount
# You can also specify the -alignment left|right option.
#
# Settings:
#   /toggle usercount_show_zero
#      Show item even when there are no users
#   /toggle usercount_show_ircops
#   /toggle usercount_show_halfops
#   /toggle usercount_show_normals
#
# You can customize the look of this item via the theme file:
#   sb_usercount = "{sb %_$0%_ nicks %c(%n$1-%c)%n}";
#   sb_uc_ircops = "%_*%_$*";
#   sb_uc_ops = "%_@%_$*";
#   sb_uc_halfops = "%_%%%_$*";
#   sb_uc_voices = "%_+%_$*";
#   sb_uc_normals = "$*";
#   sb_uc_space = " ";

my ($prefixes, $recalc, $timeout_tag, $format) = ({}, 1, 0, '');
my ($ircops, $ops, $halfops, $voices, $normals, $total);

# Called to make the statusbar item
sub usercount {
    my ($sbitem, $get_size_only) = @_;
    my $witem = Irssi::active_win()->{active};

    unless ($witem and $witem->{type} eq 'CHANNEL'
	    and defined $prefixes->{$witem->{server}{tag}}) {
	# Only works on channels
	$sbitem->{min_size} = $sbitem->{max_size} = $recalc = 0;
	return;
    }

    if ($recalc) {
	$recalc = 0;
	calc_users($witem);
	format_item();
    }

    $sbitem->default_handler($get_size_only, $format, '', 1);
}

sub calc_users {
    my ($channel) = @_;

    $ircops = $ops = $halfops = $voices = $normals = 0;
    for ($channel->nicks()) {
	if ($_->{serverop}) {
	    $ircops++;
	}

	if ($_->{op}) {
	    $ops++;
	} elsif ($_->{halfop}) {
	    $halfops++;
	} elsif ($_->{voice}) {
	    $voices++;
	} else {
	    $normals++;
	}
    }

    $total = $ops + $halfops + $voices + $normals;

    unless (Irssi::settings_get_bool('usercount_show_zero')) {
	$ircops = undef if ($ircops == 0);
	$ops = undef if ($ops == 0);
	$halfops = undef if ($halfops == 0);
	$voices = undef if ($voices == 0);
	$normals = undef if ($normals == 0);
    }

    unless (Irssi::settings_get_bool('usercount_show_halfops')
	    and $prefixes->{$channel->{server}{tag}} =~ /%/) {
	$halfops = undef;
    }

    $ircops = undef unless Irssi::settings_get_bool('usercount_show_ircops');
    $normals = undef unless Irssi::settings_get_bool('usercount_show_normals');
}

sub format_item {
    if (Irssi::current_theme()->format_expand('{sb_usercount}')) {
	# Use theme-specific look
	$format = '{sb_usercount '.$total.' ';
	$format .= '{sb_uc_ircops '.$ircops.'}{sb_uc_space}' if (defined $ircops);
	$format .= '{sb_uc_ops '.$ops.'}{sb_uc_space}' if (defined $ops);
	$format .= '{sb_uc_halfops '.$halfops.'}{sb_uc_space}' if (defined $halfops);
	$format .= '{sb_uc_voices '.$voices.'}{sb_uc_space}' if (defined $voices);
	$format .= '{sb_uc_normals '.$normals.'}' if (defined $normals);
	$format =~ s/\{sb_uc_space\}$//;
	$format .= '}';
    } else {
	# Use the default look
	$format = '{sb %_'.$total.'%_ nicks %c(%n';
	$format .= '*'.$ircops.' ' if (defined $ircops);
	$format .= '@'.$ops.' ' if (defined $ops);
	$format .= '%%'.$halfops.' ' if (defined $halfops);
	$format .= '+'.$voices.' ' if (defined $voices);
	$format .= $normals if (defined $normals);
	$format =~ s/ $//;
	$format .= '%c)%n}';
    }
}

sub refresh {
    $timeout_tag = 0 if $timeout_tag;
    $recalc = 1;
    Irssi::statusbar_items_redraw('usercount');
}

sub refresh_timed {
    Irssi::timeout_remove($timeout_tag) if $timeout_tag;
    $timeout_tag = Irssi::timeout_add_once(500, 'refresh', '');
}

sub refresh_check {
    my ($channel) = @_;
    my $witem = Irssi::active_win()->{active};

    if ($witem and $witem->{name} eq $channel->{name}
	    and $witem->{server}{tag} eq $channel->{server}{tag}) {
	refresh_timed();
    }
}

sub on_connected {
    my ($server) = @_;
    $prefixes->{$server->{tag}} = $server->isupport('PREFIX');
}

sub on_disconnected {
    my ($server) = @_;
    delete $prefixes->{$server->{tag}};
}

Irssi::settings_add_bool('usercount', 'usercount_show_zero', 1);
Irssi::settings_add_bool('usercount', 'usercount_show_ircops', 0);
Irssi::settings_add_bool('usercount', 'usercount_show_halfops', 1);
Irssi::settings_add_bool('usercount', 'usercount_show_normals', 1);

Irssi::statusbar_item_register('usercount', '', 'usercount');

Irssi::signal_add({
    'event connected' => 'on_connected',
    'server disconnected' => 'on_disconnected',
});

Irssi::signal_add_last({
    'nicklist new' => 'refresh_check',
    'nicklist remove' => 'refresh_check',
    'nick mode changed' => 'refresh_check',
    'setup changed' => 'refresh',
    'window changed' => 'refresh',
    'window item changed' => 'refresh',
});

foreach my $server (Irssi::servers()) {
    on_connected($server);
}
