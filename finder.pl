use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Finder',
    description => 'Finds, lists, or counts matching users in channels.',
    license     => 'GNU GPL v3 or later'
);

sub find {
    my ($data, $server, $witem) = @_;
    my (@args, @nicks) = (split(' ', $data));

    unless (@args && ($witem && $witem->{type} eq 'CHANNEL')) {
	return;
    }

    foreach my $arg (@args) {
	if ($witem->nick_find($arg)) {
	    push(@nicks, $arg);
	}
    }

    $witem->printformat(MSGLEVEL_CLIENTCRAP, 'nick_channels', $witem->{name}, join(' ', @nicks));
}

sub find_all {
    my ($data, $server, $witem) = @_;
    my (@args, @channels) = (split(' ', $data));

    unless (@args) {
	if ($witem && $witem->{type} eq 'QUERY') {
	    @args = ($witem->{name});
	} else {
	    return;
	}
    }

    foreach my $channel (Irssi::channels()) {
	if ($channel->nick_find($args[0])) {
	    push(@channels, $channel->{name});
	}
    }

    if ($witem) {
	$witem->printformat(MSGLEVEL_CLIENTCRAP, 'nick_channels', $args[0], join(' ', @channels));
    } else {
	Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'nick_channels', $args[0], join(' ', @channels));
    }
}

sub find_all_ext {
    my ($data, $server, $witem) = @_;
    my ($nicks, @args, @hosts, $host) = ({}, split(' ', $data));

    if (@args && ($witem && $witem->{type} eq 'CHANNEL')) {
	my $nick = $witem->nick_find($args[0]);
	if ($nick) {
	    $host = $nick->{host};
	    $host =~ s/^.*@//;
	    push(@hosts, $host);
	}
    } elsif ($witem && $witem->{type} eq 'QUERY') {
	unless (@args) {
	    @args = ($witem->{name});
	    $host = $witem->{address};
	    $host =~ s/^.*@//;
	    push(@hosts, $host);
	} elsif ($args[0] eq $witem->{name}) {
	    $host = $witem->{address};
	    $host =~ s/^.*@//;
	    push(@hosts, $host);
	}
    } else {
	return;
    }

    my $nickm = $args[0] =~ s/([^0-9])[0-9]+$/$1/r;
    foreach my $channel (Irssi::channels()) {
	foreach my $nick ($channel->nicks()) {
	    if ($nick->{nick} =~ m/^${nickm}[[:punct:]0-9]*$/i || $nickm =~ m/^\Q$nick->{nick}\E[[:punct:]0-9]*$/i
		    || (keys(%{$nicks}) && grep {my $nkm = $_ =~ s/([^0-9])[0-9]+$/$1/r; $nick->{nick} =~ m/^\Q$nkm\E[[:punct:]0-9]*$/i
			|| $nkm =~ m/^\Q$nick->{nick}\E[[:punct:]0-9]*$/i} keys(%{$nicks}))) {
		push(@{$nicks->{$nick->{nick}}{channels}}, $channel->{name});
		if ($nick->{host} && ! (@hosts && grep {$nick->{host} =~ m/@\Q$_\E$/i} @hosts)) {
		    $host = $nick->{host};
		    $host =~ s/^.*@//;
		    push(@hosts, $host);
		}
	    } elsif ($nick->{host} && (@hosts && grep {$nick->{host} =~ m/@\Q$_\E$/i} @hosts)) {
		push(@{$nicks->{$nick->{nick}}{channels}}, $channel->{name});
	    }
	}
    }

    unless (keys(%{$nicks})) {
	@{$nicks->{$args[0]}{channels}} = ();
    }

    if ($witem) {
	foreach my $nick (keys(%{$nicks})) {
	    $witem->printformat(MSGLEVEL_CLIENTCRAP, 'nick_channels', $nick, join(' ', @{$nicks->{$nick}{channels}}));
	}
    } else {
	foreach my $nick (keys(%{$nicks})) {
	    Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'nick_channels', $nick, join(' ', @{$nicks->{$nick}{channels}}));
	}
    }
}

sub list {
    my ($data, $server, $witem, $type) = @_;
    my @args = split(' ', $data);

    unless (@args && ($witem && $witem->{type} eq 'CHANNEL')) {
	return;
    }

    foreach my $nick ($witem->nicks()) {
	if ($nick->{$type} =~ m/$args[0]/i) {
	    $witem->printformat(MSGLEVEL_CLIENTCRAP, 'nick_info', $nick->{nick}, $nick->{host});
	}
    }
}

sub count {
    my ($data, $server, $witem, $type) = @_;
    my ($count, @args) = (0, split(' ', $data));

    unless (@args && ($witem && $witem->{type} eq 'CHANNEL')) {
	return;
    }

    foreach my $nick ($witem->nicks()) {
	if ($nick->{$type} =~ m/$args[0]/i) {
	    $count++;
	}
    }

    $witem->print($args[0] .': '. $count, MSGLEVEL_CLIENTCRAP);
}

sub count_all {
    my ($data, $server, $witem, $type) = @_;
    my @args = split(' ', $data);

    unless (@args) {
	return;
    }

    foreach my $channel (Irssi::channels()) {
	my $count = 0;

	foreach my $nick ($channel->nicks()) {
	    if ($nick->{$type} =~ m/$args[0]/i) {
		$count++;
	    }
	}

	if ($count) {
	    if ($witem) {
		$witem->print($channel->{name} .': '. $count, MSGLEVEL_CLIENTCRAP);
	    } else {
		Irssi::print($channel->{name} .': '. $count, MSGLEVEL_CLIENTCRAP);
	    }
	}
    }
}

sub list_nick {
    my ($data, $server, $witem) = @_;
    list($data, $server, $witem, 'nick');
}

sub list_host {
    my ($data, $server, $witem) = @_;
    list($data, $server, $witem, 'host');
}

sub count_nick {
    my ($data, $server, $witem) = @_;
    count($data, $server, $witem, 'nick');
}

sub count_host {
    my ($data, $server, $witem) = @_;
    count($data, $server, $witem, 'host');
}

sub count_nick_all {
    my ($data, $server, $witem) = @_;
    count_all($data, $server, $witem, 'nick');
}

sub count_host_all {
    my ($data, $server, $witem) = @_;
    count_all($data, $server, $witem, 'host');
}

Irssi::theme_register([
    nick_info => '{nick $0} {nickhost $1}',
    nick_channels => '$0: {channel $1}'
]);

Irssi::command_bind({
    find    => 'find',           ic   => 'find',
    finda   => 'find_all',       ica  => 'find_all',
    findax  => 'find_all_ext',   icax => 'find_all_ext',
    listn   => 'list_nick',      ln   => 'list_nick',
    listh   => 'list_host',      lh   => 'list_host',
    countn  => 'count_nick',     cn   => 'count_nick',
    counth  => 'count_host',     ch   => 'count_host',
    countna => 'count_nick_all', cna  => 'count_nick_all',
    countha => 'count_host_all', cha  => 'count_host_all'
}, 'finder');
