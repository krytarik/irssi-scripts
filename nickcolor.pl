use strict;
use warnings;
use Irssi;

our $VERSION = '3';
our %IRSSI = (
    authors	=> 'Timo Sirainen, Ian Peters, David Leadbeater, Krytarik Raido',
    contact	=> 'tss@iki.fi, krytarik@tuxgarage.com',
    url		=> 'https://bitbucket.org/krytarik/irssi-scripts',
    name	=> 'Nick Color',
    description	=> 'Assign a different color for each nick',
    license	=> 'Public Domain',
    changed	=> '2017-12-03'
);

# Settings:
#   nickcolor_colors: List of color codes to use,
#     e.g. /set nickcolor_colors 2 3 4 5 6 7 9 10 11 12 13
#     (avoid 8, as used for hilights in the default theme).
#   nickcolor_keep: Keep color throughout nick changes or not.

my %saved_colors = ();
my %session_colors = ();
my $saved_colors_file = Irssi::get_irssi_dir() . "/saved_colors";

sub load_colors {
  open my $color_fh, "<", $saved_colors_file or return;
  while (<$color_fh>) {
    chomp;
    my ($nick, $color) = split ":";
    $saved_colors{$nick} = $color;
  }
}

sub save_colors {
  open COLORS, ">", $saved_colors_file;
  foreach my $nick (keys %saved_colors) {
    print COLORS "$nick:$saved_colors{$nick}\n";
  }
  close COLORS;
}

# If someone we've colored (either through the saved colors, or the hash
# function) changes their nick, we'd like to keep the same color associated
# with them (but only in the session_colors, i.e. a temporary mapping).

sub sig_nick {
  return unless Irssi::settings_get_bool('nickcolor_keep');
  my ($server, $newnick, $nick, $address) = @_;

  if ($saved_colors{$nick}) {
    $session_colors{$newnick} = $saved_colors{$nick};
  } elsif ($session_colors{$nick}) {
    $session_colors{$newnick} = $session_colors{$nick};
  }
}

# This gave reasonable distribution values when run across
# /usr/share/dict/words

sub simple_hash {
  my ($nick) = @_;
  my $counter;

  foreach my $char (split //, $nick) {
    $counter += ord $char;
  }

  my @colors = split " ", Irssi::settings_get_str('nickcolor_colors');
  return $colors[$counter % @colors];
}

sub get_color {
  my ($nick) = @_;
  my $color;

  # Has the user assigned this nick a color?
  if ($saved_colors{$nick}) {
    $color = $saved_colors{$nick}
  }

  # Have -we- already assigned this nick a color?
  elsif ($session_colors{$nick}) {
    $color = $session_colors{$nick};
  }

  # Let's assign this nick a color
  else {
    $color = $session_colors{$nick} = simple_hash($nick);
  }

  return sprintf "\003%02d", $color;
}

sub sig_public {
  my ($server, $msg, $nick, $address, $target) = @_;
  $server->command('^format pubmsg {pubmsgnick $2 {pubnick ' . get_color($nick) . '$0}}$1');
  $server->command('^format pubmsg_channel {pubmsgnick $3 {pubnick ' . get_color($nick) . '$0}{msgchannel $1}}$2');
}

sub sig_action {
  my ($server, $msg, $nick, $address, $target) = @_;
  $server->command('^format action_public {pubaction ' . get_color($nick) . '$0}$1');
  $server->command('^format action_public_channel {pubaction ' . get_color($nick) . '$0{msgchannel $1}}$2');
}

sub cmd_color {
  my ($data, $server, $witem) = @_;
  my ($op, $nick, $color) = split " ", $data;
  $op = lc $op;

  if (!$op) {
    Irssi::print ("No operation given (save/set/clear/list/preview)");
  } elsif ($op eq "save") {
    save_colors;
  } elsif ($op eq "set") {
    if (!$nick) {
      Irssi::print ("Nick not given");
    } elsif (!$color) {
      Irssi::print ("Color not given");
    } elsif ($color < 2 || $color > 14) {
      Irssi::print ("Color must be between 2 and 14 inclusive");
    } else {
      $saved_colors{$nick} = $color;
    }
  } elsif ($op eq "clear") {
    if (!$nick) {
      Irssi::print ("Nick not given");
    } else {
      delete ($saved_colors{$nick});
    }
  } elsif ($op eq "list") {
    Irssi::print ("Saved colors:");
    foreach my $nick (keys %saved_colors) {
      Irssi::print (chr (3) . "$saved_colors{$nick}$nick" .
		    chr (3) . "1 ($saved_colors{$nick})");
    }
  } elsif ($op eq "preview") {
    Irssi::print ("Available colors:");
    foreach my $i (2..14) {
      Irssi::print (chr (3) . $i . "Color #$i");
    }
  }
}

load_colors;

Irssi::settings_add_str('nickcolor', 'nickcolor_colors', '2 3 4 5 6 7 9 10 11 12 13');
Irssi::settings_add_bool('nickcolor', 'nickcolor_keep', 1);
Irssi::command_bind('color', 'cmd_color');

Irssi::signal_add({
  'message public' => 'sig_public',
  'message irc op_public' => 'sig_public',
  'message irc action' => 'sig_action',
  'message nick' => 'sig_nick'
});
