#
# This little script will do just one thing: it will draw a line each time you
# switch away from a window. This way, you always know just up to where you've
# been reading that window :) It also removes the previous drawn line, so you
# don't see double lines.
#
# Note: Redraw trackbar only works on Irssi 0.8.17 and later.
#

use strict;
use warnings;
use Irssi;
use Irssi::TextUI;
use Encode;
use POSIX qw(strftime);

our $VERSION = '2.3';
our %IRSSI = (
    authors     => 'Peter Leurs, Geert Hauwaerts, Krytarik Raido',
    contact     => 'peter@pfoe.be, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Trackbar',
    description => 'Shows a bar where you have last read a window.',
    license     => 'GNU GPL v2 or later',
    changed     => '2017-03-10',
);

## Settings.
#
# The script works right out of the box, but if you want you can change
# how it works by /set'ing the following variables:
#
#    trackbar_style
#      - This is the color that your trackbar will be in.
#        Only Irssi color formats are allowed. If you don't know
#        the color formats by heart, you can take a look at the
#        formats documentation at https://irssi.org/documentation
#        The default setting is '%K'.
#
#    trackbar_string
#      - This is the string that your trackbar will display. This can
#        be multiple characters or just one. For example: '~-~-'
#        The default setting is '-'.
#
#    trackbar_use_status_window
#      - If this setting is set to OFF, Irssi won't print a trackbar
#        in the status window.
#
#    trackbar_ignore_windows
#      - A list of windows where no trackbar should be printed.
#
#    trackbar_print_timestamp
#      - If this setting is set to ON, Irssi will print a formatted
#        timestamp in front of the trackbar.
#
#    trackbar_require_seen
#      - Only clear the trackbar if it has been scrolled to.
#
##

## Commands.
#
#    /trackbar, /trackbar goto
#      - Jump to where the trackbar is, to pick up reading.
#
#    /trackbar keep
#      - Keep this window's trackbar where it is the next time
#        you switch windows (then this flag is cleared again).
#
#    /mark, /trackbar mark
#      - Remove the old trackbar and mark the bottom of this
#        window with a new trackbar.
#
#    /trackbar markvisible
#      - Like mark for all visible windows.
#
#    /trackbar markall
#      - Like mark for all windows.
#
#    /trackbar remove
#      - Remove this window's trackbar.
#
#    /trackbar removeall
#      - Remove all windows' trackbars.
#
#    /trackbar redraw
#      - Force redraw of trackbars.
#
##

## Comments and remarks.
#
# This script uses settings.
# Use /SET to change the value or /TOGGLE to switch it on or off.
#
#    Tip:     The command 'trackbar' is very useful if you bind that to a key,
#             so you can easily jump to the trackbar. Please see '/help bind' for
#             more information on key bindings in Irssi.
#
#    Command: /BIND meta2-P key F1
#             /BIND F1 command trackbar
#
##

## Bug fixes and new features in this rewrite.
#
#   - Remove all the trackbars before upgrading.
#   - New setting trackbar_use_status_window to control the status window trackbar.
#   - New setting trackbar_print_timestamp to print a timestamp or not.
#   - When resizing the terminal, Irssi will update all the trackbars to the new size.
#   - When changing trackbar settings, change all the trackbars to the new settings.
#   - New command 'trackbar' to scroll up to the trackbar.
#   - New command 'trackbar mark' to draw a new trackbar (the old '/mark').
#   - New command 'trackbar markall' to draw a new trackbar in each window.
#   - New command 'trackbar remove' to remove the trackbar from the current window.
#   - New command 'trackbar removeall' to remove all the trackbars.
#   - New command 'trackbar help' to display the trackbar commands.
#   - Don't draw a trackbar in empty windows.
#   - Added a version check to prevent Irssi redraw errors.
#   - Fixed a bookmark NULL versus 0 bug.
#   - Fixed a remove-line bug in Uwe Dudenhoeffer's patch.
#   - Fixed an Irssi startup bug, now processing each auto-created window.
#
##

## Known bugs and todo list.
#
#    Todo: * Instead of drawing a line, invert the line.
#
##

sub cmd_help {
    print CLIENTCRAP <<HELP;

%9Syntax:%9

TRACKBAR
TRACKBAR GOTO
TRACKBAR KEEP
TRACKBAR MARK
TRACKBAR MARKVISIBLE
TRACKBAR MARKALL
TRACKBAR REMOVE
TRACKBAR REMOVEALL
TRACKBAR REDRAW

%9Parameters:%9

    GOTO:        Jump to where the trackbar is, to pick up reading.
    KEEP:        Keep this window's trackbar where it is the next time
                 you switch windows (then this flag is cleared again).
    MARK:        Remove the old trackbar and mark the bottom of this
                 window with a new trackbar.
    MARKVISIBLE: Like mark for all visible windows.
    MARKALL:     Like mark for all windows.
    REMOVE:      Remove this window's trackbar.
    REMOVEALL:   Remove all windows' trackbars.
    REDRAW:      Force redraw of trackbars.

%9Description:%9

    Manage a trackbar. Without arguments, it will scroll up to the trackbar.

%9Examples:%9

    /TRACKBAR MARK
    /TRACKBAR REMOVE
HELP
}

my (%config, %keep_trackbar, %unseen_trackbar);
my $old_irssi = Irssi::version < 20140701;
my $text_charwidth = eval { require Text::CharWidth };
my %strip_table = (
    (map { $_ => '' } (split //, '04261537' . 'kbgcrmyw' . 'KBGCRMYW' . 'U9_8I:|FnN>#[' . 'pP')),
    (map { $_ => $_ } (split //, '{}%')),
);

sub remove_one_trackbar {
    my $win = shift;
    my $view = shift || $win->view;
    my $line = $view->get_bookmark('trackbar');
    if (defined $line) {
        my $bottom = $view->{bottom};
        $view->remove_line($line);
        $win->command('^scrollback end') if $bottom && !$win->view->{bottom};
        $view->redraw;
    }
}

sub add_one_trackbar {
    my $win = shift;
    my $view = shift || $win->view;
    $win->print(line($win->{width}), MSGLEVEL_NEVER);
    $view->set_bookmark_bottom('trackbar');
    $unseen_trackbar{ $win->{_irssi} } = 1;
    $view->redraw;
}

sub update_one_trackbar {
    my $win = shift;
    my $view = shift || $win->view;
    my $force = shift;
    my $ignored = win_ignored($win, $view);
    remove_one_trackbar($win, $view)
        if $force || !defined $force || !$ignored;
    add_one_trackbar($win, $view)
        if $force || !$ignored;
}

sub win_ignored {
    my $win = shift;
    my $view = shift || $win->view;
    return 1 if (!$view->{buffer}{lines_count}
        || ($win->{name} eq '(status)' && !$config{use_status_window})
        || grep { $win->{name} eq $_ || $win->{refnum} eq $_
            || $win->get_active_name eq $_ } @{ $config{ignore_windows} })
}

sub sig_window_changed {
    my ($newwindow, $oldwindow) = @_;
    return unless $oldwindow;
    trackbar_update_seen($newwindow);
    return if delete $keep_trackbar{ $oldwindow->{_irssi} };
    trackbar_update_seen($oldwindow);
    return if $config{require_seen} && $unseen_trackbar{ $oldwindow->{_irssi } };
    update_one_trackbar($oldwindow, undef, 0);
}

sub trackbar_update_seen {
    my $win = shift;
    return unless $win;
    my $view = $win->view;
    my $line = $view->get_bookmark('trackbar');
    unless (defined $line) {
        delete $unseen_trackbar{ $win->{_irssi} };
        return;
    }
    my $startline = $view->{startline};
    return unless $startline;
    if ($startline->{info}{time} < $line->{info}{time}
            || $startline->{_irssi} == $line->{_irssi}) {
        delete $unseen_trackbar{ $win->{_irssi} };
    }
}

sub screen_length {
    my $string = Irssi::strip_codes($_[0]);
    $string =~ s/(%(%|Z.{6}|z.{6}|X..|x..|.))/exists $strip_table{$2} ? $strip_table{$2} :
        $2 =~ m{x(?:0[a-f]|[1-6][0-9a-z]|7[a-x])|z[0-9a-f]{6}}i ? '' : $1/gex;
    if ($text_charwidth) {
        Text::CharWidth::mbswidth($string);
    } else {
        Encode::_utf8_on($string) if $config{is_utf8};
        length($string);
    }
}

sub line {
    my ($width, $time)  = @_;
    my $string = $config{string};
    $string = ' ' unless $string;
    $time ||= time;

    Encode::_utf8_on($string) if $config{is_utf8};
    my $length = screen_length($string);

    my $format = '';
    if ($config{print_timestamp}) {
        $format = $config{timestamp_str};
        $format =~ y/%/\01/;
        $format =~ s/\01\01/%/g;
        $format = strftime($format, localtime $time);
        $format =~ y/\01/%/;
    }

    my $times = $width / $length;
    $times += 1 if $times != int $times;
    $format .= $config{style};
    $width -= screen_length($format);
    $string x= $times;
    chop $string while $string && screen_length($string) > $width;
    return $format . $string;
}

sub remove_all_trackbars {
    for my $win (Irssi::windows) {
        remove_one_trackbar($win);
    }
}

sub UNLOAD {
    remove_all_trackbars();
}

sub redraw_trackbars {
    if ($old_irssi) {
        Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'trackbar_wrong_version');
        return;
    }
    for my $win (Irssi::windows) {
        my $view = $win->view;
        my $line = $view->get_bookmark('trackbar');
        next unless defined $line;
        my $bottom = $view->{bottom};
        $win->print_after($line, MSGLEVEL_NEVER, line($win->{width}, $line->{info}{time}),
                          $line->{info}{time});
        $view->set_bookmark('trackbar', $win->last_line_insert);
        $view->remove_line($line);
        $win->command('^scrollback end') if $bottom && !$win->view->{bottom};
        $view->redraw;
    }
}

sub goto_trackbar {
    my $win = Irssi::active_win;
    my $line = $win->view->get_bookmark('trackbar');
    if (defined $line) {
        $win->command("scrollback goto ". strftime("%d %H:%M:%S", localtime($line->{info}{time})));
    } else {
        $win->printformat(MSGLEVEL_CLIENTCRAP, 'trackbar_not_found');
    }
}

sub cmd_mark {
    update_one_trackbar(Irssi::active_win, undef, 1);
}

sub cmd_markall {
    for my $win (Irssi::windows) {
        update_one_trackbar($win);
    }
}

sub signal_stop {
    Irssi::signal_stop;
}

sub cmd_markvisible {
    my @wins = Irssi::windows;
    my $awin =
        my $bwin = Irssi::active_win;
    my $awin_counter = 0;
    Irssi::signal_add_priority('window changed' => 'signal_stop', -99);
    do {
        Irssi::active_win->command('window up');
        $awin = Irssi::active_win;
        update_one_trackbar($awin);
        ++$awin_counter;
    } until ($awin->{refnum} == $bwin->{refnum} || $awin_counter >= @wins);
    Irssi::signal_remove('window changed' => 'signal_stop');
}

sub cmd_trackbar_remove_one {
    remove_one_trackbar(Irssi::active_win);
}

sub cmd_remove_all_trackbars {
    remove_all_trackbars();
    Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'trackbar_all_removed');
}

sub cmd_keep_once {
    $keep_trackbar{ Irssi::active_win->{_irssi} } = 1;
}

sub trackbar_runsub {
    my ($data, $server, $item) = @_;
    $data =~ s/\s+$//g;

    if ($data) {
        Irssi::command_runsub('trackbar', $data, $server, $item);
    } else {
        goto_trackbar();
    }
}

sub update_config {
    my $was_status_window = $config{use_status_window};
    $config{style} = Irssi::settings_get_str('trackbar_style');
    $config{string} = Irssi::settings_get_str('trackbar_string');
    $config{require_seen} = Irssi::settings_get_bool('trackbar_require_seen');
    $config{ignore_windows} = [ split /[,\s]+/, Irssi::settings_get_str('trackbar_ignore_windows') ];
    $config{use_status_window} = Irssi::settings_get_bool('trackbar_use_status_window');
    $config{print_timestamp} = Irssi::settings_get_bool('trackbar_print_timestamp');
    $config{is_utf8} = Irssi::settings_get_str('term_charset') =~ m/^utf-8$/i;

    if (defined $was_status_window && $was_status_window != $config{use_status_window}) {
        if (my $swin = Irssi::window_find_name('(status)')) {
            if ($config{use_status_window}) {
                update_one_trackbar($swin);
            } else {
                remove_one_trackbar($swin);
            }
        }
    }
    if ($config{print_timestamp}) {
        my $ts_format = Irssi::settings_get_str('timestamp_format');
        my $ts_theme = Irssi::current_theme->get_format('fe-common/core', 'timestamp');
        my $render_str = Irssi::current_theme->format_expand($ts_theme);
        (my $ts_escaped = $ts_format) =~ s/([%\$])/$1$1/g;
        $render_str =~ s/(?|\$(.)(?!\w)|\$\{(\w+)\})/$1 eq 'Z' ? $ts_escaped : $1/ge;
        $config{timestamp_str} = $render_str;
    }
    redraw_trackbars() unless $old_irssi;
}

Irssi::theme_register([
    trackbar_wrong_version => '%R>>%n %_Trackbar:%_ Please upgrade your client to v0.8.17 '
                              . 'or later if you would like to use this feature.',
    trackbar_all_removed => '%R>>%n %_Trackbar:%_ All the trackbars have been removed.',
    trackbar_not_found => '%R>>%n %_Trackbar:%_ No trackbar found in this window.',
]);

Irssi::settings_add_str('trackbar', 'trackbar_string', '-');
Irssi::settings_add_str('trackbar', 'trackbar_style', '%K');
Irssi::settings_add_str('trackbar', 'trackbar_ignore_windows', '');
Irssi::settings_add_bool('trackbar', 'trackbar_use_status_window', 1);
Irssi::settings_add_bool('trackbar', 'trackbar_print_timestamp', 0);
Irssi::settings_add_bool('trackbar', 'trackbar_require_seen', 0);

update_config();

Irssi::signal_add_last('mainwindow resized' => 'redraw_trackbars')
    unless $old_irssi;

Irssi::signal_register({'gui page scrolled' => [qw/Irssi::UI::Window/]});
Irssi::signal_add_last('gui page scrolled' => 'trackbar_update_seen');

Irssi::signal_add('window changed' => 'sig_window_changed');
Irssi::signal_add('setup changed' => 'update_config');
Irssi::signal_add_priority('session save' => 'remove_all_trackbars', Irssi::SIGNAL_PRIORITY_HIGH-1);

Irssi::command_bind({
    'trackbar goto'        => 'goto_trackbar',
    'trackbar keep'        => 'cmd_keep_once',
    'trackbar mark'        => 'cmd_mark',
    'trackbar markvisible' => 'cmd_markvisible',
    'trackbar markall'     => 'cmd_markall',
    'trackbar remove'      => 'cmd_trackbar_remove_one',
    'trackbar removeall'   => 'cmd_remove_all_trackbars',
    'trackbar redraw'      => 'redraw_trackbars',
    'trackbar help'        => 'cmd_help',
    'trackbar'             => 'trackbar_runsub',
    'mark'                 => 'cmd_mark',
});
