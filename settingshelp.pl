use strict;
use warnings;
use LWP::Simple;
use Irssi;

our $VERSION = '1.5';
our %IRSSI = (
    authors     => 'Rocco Caputo (dngor), Nei, Krytarik Raido',
    contact     => 'rcaputo@cpan.org, Nei @ anti@conference.jabber.teamidiot.de, krytarik@tuxgarage.com',
    url         => 'https://irssi.org/documentation/settings/',
    name        => 'Settings Help',
    description => 'Irssi settings documentation',
    license     => 'CC BY-SA 2.5 <https://creativecommons.org/licenses/by-sa/2.5/>',
);

# Usage
# =====
# Now you can do:
#
#   /help set <setting>
#   /help set app <appendix>
#   /help set [app] update
#
# to print out its documentation (as far as it was documented on the
# Irssi website)

# Notes
# =====
# This script will download the settings documentation from the
# website every time it is loaded.

my %help;

sub get_docs {
    local $LWP::Simple::ua->{timeout} = 2;
    my $res = LWP::Simple::get("https://github.com/irssi/irssi.github.io/raw/master/documentation/settings.markdown");
    my @res = split "\n", $res if defined $res;
    unless (@res && $res[0] eq '---') {
	print CLIENTERROR "Error downloading settings documentation";
	return 0;
    }
    while (@res) {
	my ($type, $setting, $appendix, $default, $appname, @description);
	while (defined(my $line = shift @res)) {
	    if (!$type) {
		if ($line =~ /^\{:#\w+\}$/) {
		    $type = "setting";
		}
		elsif ($line eq '* * *') {
		    $type = "appendix";
		}
	    }
	    elsif (!$setting && !$appendix) {
		if ($type eq 'setting' && $line =~ /^` (\w+)` \*\*`([^\s`]*)\s*`\*\*$/) {
		    ($setting, $default) = ($1, $2);
		    $default =~ s/%/%%/g;
		}
		elsif ($type eq 'appendix' && $line =~ /^##\s+Appendix \w+: ((\S+).*?)\s*$/) {
		    ($appendix, $appname) = ("\L$2\E", $1);
		}
	    }
	    elsif ($setting && $line =~ /^>+(?:\s|$)/) {
		next if $line =~ /^>>+$/;
		$line =~ s/^>+\s?//;
		push @description, filter_text($line);
	    }
	    elsif ($appendix && $line ne '* * *') {
		if (!@description) {
		    next if $line =~ /^(?:\{:#\w+\}|)$/;
		}
		elsif (!$description[-1]) {
		    next unless $line;
		}
		push @description, filter_text($line);
	    }
	    elsif (@description) {
		unshift @res, $line;
		last;
	    }
	}
	if (@description) {
	    if ($setting) {
		@{$help{settings}{$setting}} = ($setting, $default, @description);
	    } else {
		pop @description unless $description[-1];
		@{$help{appendices}{$appendix}} = ($appendix, $appname, @description);
	    }
	}
    }
    print CLIENTNOTICE "Downloaded settings documentation";
    return 1;
}

sub filter_text {
    my ($text) = @_;
    $text =~ s/`//g;
    $text =~ s/%/%%/g;
    $text =~ s/^\*\s/    * /;
    $text =~ s/!?\[(.*)\]\((\S+)\)/@{[$1 ? "%U$1%U ":""]}<@{[$2 !~ m|:\/\/| ? $IRSSI{url}:""]}$2>/g;
    return $text;
}

sub print_info {
    print CLIENTCRAP <<"INFO";

%U%_Irssi settings documentation%_%U
$IRSSI{license}

Get help on a setting: %_/help set <setting>%_
Print an appendix:     %_/help set app <appendix>%_
Update documentation:  %_/help set [app] update%_
INFO
}

sub print_help_setting {
    my ($setting, $default, @description) = @_;
    s/^[\s*]*/    $&%|/ for @description;
    print CLIENTCRAP <<"HELP";

%_Setting:%_ $setting

%_Default:%_ $default

%_Description:%_

@{[join "%:", @description]}
HELP
}

sub print_help_appendix {
    my ($appendix, $appname, @description) = @_;
    s/(?:^[\s*|]*|\s{3,})/$&%|/g for @description;
    print CLIENTCRAP <<"HELP";

%_Appendix:%_ $appname

@{[join "%:", @description]}
HELP
}

sub help_set_app {
    my ($data) = @_;
    my @args = split(' ', "\L$data\E");
    if (@args && (%help || get_docs()) && $help{appendices}{$args[0]}) {
	Irssi::signal_stop();
	print_help_appendix(@{$help{appendices}{$args[0]}});
    }
    elsif ($args[0] eq 'update') {
	get_docs();
	Irssi::signal_stop();
    }
}

Irssi::signal_add_first(
    'command help' => sub {
	my ($data) = @_;
	my @args = split(' ', "\L$data\E");
	if (scalar @args >= 2 && $args[0] eq 'set') {
	    if ($args[1] eq 'app') {
		if (scalar @args >= 3) {
		    help_set_app($args[2]);
		}
	    }
	    elsif (%help || get_docs()) {
		if ($help{settings}{$args[1]}) {
		    Irssi::signal_stop();
		    print_help_setting(@{$help{settings}{$args[1]}});
		}
		elsif ($help{appendices}{$args[1]}) {
		    Irssi::signal_stop();
		    print_help_appendix(@{$help{appendices}{$args[1]}});
		}
		elsif ($args[1] eq 'update') {
		    get_docs();
		    Irssi::signal_stop();
		}
	    }
	}
    }
);

Irssi::settings_add_bool('settingshelp', 'settingshelp_info', 1);
print_info() if Irssi::settings_get_bool('settingshelp_info');

Irssi::signal_register({
    'complete command ' => [qw[glistptr_char* Irssi::UI::Window string string intptr]]
});

Irssi::signal_add_last(
    'complete command help' => sub {
	my ($cl, $win, $word, $start, $ws) = @_;
	if ($start =~ /^set$/i) {
	    Irssi::signal_emit('complete command set', $cl, $win, $word, '', $ws);
	}
    }
);

Irssi::command_bind({
    'help set app' => 'help_set_app',
    'help set update' => 'get_docs',
    'help set app update' => 'get_docs',
});
