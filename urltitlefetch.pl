#
# This Irssi script automatically fetches and prints
# the HTML titles from website URLs found in messages
# and actions.
#
# Additionally, it notifies about any found image, audio, and
# document URLs, as well as any other possible URIs, and reprints
# any found URIs that would otherwise be non-clickable because of some
# directly attached punctuation characters, plus bare "www.[..]" ones.
#
# Requires: 'libwww-perl'
#

use strict;
use warnings;
require LWP::UserAgent;
use HTML::Entities;
use Irssi;

our $VERSION = '0.9';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'URL Title Fetch',
    description => 'Fetches and prints the titles from website URLs.',
    license     => 'GNU GPL v3 or later'
);

sub urltitle_get {
    my ($server, $text, $nick, $address, $target) = (@_, '');
    my @blacklist = split(',', Irssi::settings_get_str('urltitlefetch_blacklist'));
    unless ((@blacklist && grep {$nick =~ m/$_/i || $address =~ m/$_/i} @blacklist) || ! $text) {
	my $minslash = "2";   # "0": match *all* URIs // "2": match only URLs
	my @uris = $text =~ m/([[:punct:]]*[[:alpha:]]+[[:alnum:]\+\-\.]*:[\/]{$minslash,3}[[:alnum:]][^ ]+|[[:punct:]]*www\.[[:alnum:]][^ ]*\.[^ ]+)/g;
	my ($buffer, $urinick);
	if (@uris) {
	    Irssi::signal_continue(@_);
	    if ($target =~ m/^[@+]?([#&]\S+)$/) {
		$buffer = $1;
		$urinick = "%K< %r[uri]%K>%n %|";   # depends on theme
	    } else {
		$buffer = $nick;
		$urinick = "%K<%r[uri]%K>%n %|";   # depends on theme
	    }
	}
	foreach my $uri (@uris) {
	    my $title;
	    my $uriclk = "";
	    $uri =~ s/[,;.]+$//;
	    if ($uri =~ m/^[\(\<\[\{](.*?)[\)\>\]\}]$/) {
		$uri = $1;
	    }
	    if ($uri =~ m/^[^\(]+\)+$/) {
		$uri =~ s/\)+$//;
	    }
	    if ($uri =~ m/^[^\<]+\>+$/) {
		$uri =~ s/\>+$//;
	    }
	    if ($uri =~ m/^[^\[]+\]+$/) {
		$uri =~ s/\]+$//;
	    }
	    if ($uri =~ m/^[^\{]+\}+$/) {
		$uri =~ s/\}+$//;
	    }
	    $uri =~ s/^[\(\<\[\{]+//;
	    if ($uri =~ m/[\+\-\.]{2,}[[:alnum:]\+\-\.]*:/) {
		$uri =~ m/[\+\-\.]{2,}([[:alnum:]\+\-\.]*:.*?)$/;
		$uri = $1;
		$uriclk = $uri;
	    }
	    if ($uri =~ m/\([^\(\)]*\)/) {
		$uri =~ s/\(([^\(\)]*)\)/%28$1%29/g;
		$uriclk = $uri;
	    }
	    if ($uri =~ m/(^[[:punct:]]+|[^[:alnum:]\/]+$)/) {
		$uri =~ m/^[[:punct:]]*(.*?)[^[:alnum:]\/]*$/;
		$uri = $1;
		$uriclk = $uri;
	    }
	    if ($uri =~ m/^www\./) {
		$uri = "http://".$uri;
		$uriclk = $uri;
	    }
	    if ($uri =~ m/\.(bmp|gif|jpe?g|png|tiff?|webp)$/i) {
		$title = "Image";
	    }
	    elsif ($uri =~ m/\.(aac|aiff?|asf|au|flac|m4a|mp3|ogg|opus|ram?|wav|wma)$/i) {
		$title = "Audio";
	    }
	    elsif ($uri =~ m/\.(txt|rtf|pdf|docx?|xlsx?|pp[ts]x?|od[tsp])$/i) {
		$title = "Document";
	    }
	    elsif ($uri =~ m/\.(iso|img(\.[gx]z)?|tar(\.([gx]z|bz2))?|7z|rar|zip|xz|deb|rpm)$/i) {
		$title = "Archive";
	    }
	    elsif ($uri =~ m/^https?:\/\/[a-z]?[ph]asti?e/) {
		$title = "Paste";
	    }
	    elsif ($uri =~ m/^https?:\/\//) {
		my $uagent = LWP::UserAgent->new(
		    agent    => Irssi::settings_get_str('urltitlefetch_useragent'),
		    ssl_opts => {verify_hostname => 0},   # verify hostname against certificate
		    max_size => 16100,   # download size limit in bytes
		    timeout  => 10   # timeout of no activity in seconds
		);
		my $result = $uagent->get($uri);
		if ($result->is_success) {
		    my $content = $result->decoded_content;
		    if (! $content) {
			$title = "Other";
		    }
		    elsif ($content =~ m/<title[^>]*>[[:space:]]*([\S\s]+?)[[:space:]]*<\/title>/i) {
			$title = decode_entities($1);
			$title =~ s/\v//g;
			$title =~ s/\h+/ /g;
			if (length($title) > 120) {
			    $title = substr($title, 0, 117);
			    $title =~ s/[ ]+$//;
			    $title .= "...";
			}
		    }
		    else {
			if ($content =~ m/<html[^>]*>/i) {
			    $title = "No Title";
			}
			else {
			    $title = "Other";
			}
		    }
		}
		else {
		    $title = $result->status_line;
		}
	    }
	    else {
		$title = "Other";
	    }
	    $title =~ s/%/%%/g;
	    $server->print($buffer, $urinick . $title, MSGLEVEL_CLIENTCRAP);
	    if ($uriclk) {
		$uriclk =~ s/%/%%/g;
		$server->print($buffer, $urinick . $uriclk, MSGLEVEL_CLIENTCRAP);
	    }
	}
    }
}

Irssi::signal_add_last('message public', 'urltitle_get');
Irssi::signal_add_last('message irc op_public', 'urltitle_get');
Irssi::signal_add_last('message private', 'urltitle_get');
Irssi::signal_add_last('message irc action', 'urltitle_get');

Irssi::settings_add_str('urltitlefetch', 'urltitlefetch_blacklist',
    'ubot(tu|[0-9]+),kubot,le_bot,dpkg,judd,GitHub,Not-[0-9a-z]+,AntiSpamMeta,@ubuntu/bot/,@192\.30\.252\.[0-9]+');
Irssi::settings_add_str('urltitlefetch', 'urltitlefetch_useragent',
    'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:50.0) Gecko/20100101 Firefox/50.0');
