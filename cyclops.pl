use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Cyclops',
    description => 'Detects and notifies about excessive cycling.',
    license     => 'GNU GPL v3 or later'
);

# period    = period in minutes
# garbage   = period in minutes
# threshold = number of cycle events,
#             or zero to disable
# verify    = verify alternative nicks by address;
#             'full', 'user', or empty to not
# ignores   = comma-separated list of channels
# target    = report to '<channel>[/<server>]',
#             or empty to just notify

my $cycles = {};

sub cyclops {
    my ($server, $channel, $nick, $newnick, $address, $type) = @_;
    my ($servtag, $time) = ($server->{tag}, time());

    unless (@{$cycles->{$servtag}{$nick}{addresses} || []}
	    && grep {/^\Q$address\E$/i} @{$cycles->{$servtag}{$nick}{addresses}}) {
	push(@{$cycles->{$servtag}{$nick}{addresses}}, $address);
    }

    if ($type eq 'join') {
	if (@{$cycles->{$servtag}{$nick}{channels}{$channel} || []}) {
	    @{$cycles->{$servtag}{$nick}{channels}{$channel}}[-1] = $time;
	} else {
	    @{$cycles->{$servtag}{$nick}{channels}{$channel}} = ($time);
	}
	return;
    }

    unless ($cycles->{$servtag}{$nick}{initial}) {
	$cycles->{$servtag}{$nick}{initial} = $time;
    }

    if ($type eq 'nick') {
	if (@{$cycles->{$servtag}{$nick}{altnicks} || []}) {
	    @{$cycles->{$servtag}{$nick}{altnicks}} = grep {$_ ne $nick}
		@{$cycles->{$servtag}{$nick}{altnicks}};
	    if (scalar(@{$cycles->{$servtag}{$nick}{altnicks}}) > 5) {
		@{$cycles->{$servtag}{$nick}{altnicks}} =
		    @{$cycles->{$servtag}{$nick}{altnicks}}[-5..-1];
	    }
	}
	push(@{$cycles->{$servtag}{$nick}{altnicks}}, $nick);
    }

    push(@{$cycles->{$servtag}{$nick}{channels}{$channel}}, $time);
    my @cycleall = @{$cycles->{$servtag}{$nick}{channels}{$channel}};
    my ($initial, $report) = ($cycles->{$servtag}{$nick}{initial}) x 2;
    if ($cycles->{$servtag}{$nick}{report}) {
	$report = $cycles->{$servtag}{$nick}{report};
    }

    my @chanuser = keys(%{$cycles->{$servtag}{$nick}{channels}});
    my ($verify, @altnicks) = (Irssi::settings_get_str('cyclops_verify'));
    my $nickm = $nick =~ s/([^0-9])[0-9]+$/$1/r;
    foreach my $altnick (grep {($_ =~ m/^\Q$nickm\E[[:punct:]0-9]+$/i
	    || $nickm =~ m/^\Q$_\E[[:punct:]0-9]+$/i) && $_ ne $nick} keys(%{$cycles->{$servtag}})) {
	if (! $verify || ($verify eq 'user' && grep {$_ =~ m/^\~?([^@]+)/; grep {/^\~?\Q$1\E/i}
		    @{$cycles->{$servtag}{$altnick}{addresses}}} @{$cycles->{$servtag}{$nick}{addresses}})
		|| ($verify eq 'full' && grep {my $addr = $_; grep {/^\Q$addr\E$/i}
		    @{$cycles->{$servtag}{$altnick}{addresses}}} @{$cycles->{$servtag}{$nick}{addresses}})) {
	    push(@altnicks, $altnick);
	    if (my @chans = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @chanuser}
		    keys(%{$cycles->{$servtag}{$altnick}{channels}})) {
		push(@chanuser, @chans);
	    }
	    if ($cycles->{$servtag}{$altnick}{initial}
		    && $cycles->{$servtag}{$altnick}{initial} < $initial) {
		if ($report == $initial) {
		    $report = $cycles->{$servtag}{$altnick}{initial};
		}
		$initial = $cycles->{$servtag}{$altnick}{initial};
	    }
	    if ($cycles->{$servtag}{$altnick}{report}
		    && $cycles->{$servtag}{$altnick}{report} > $report) {
		$report = $cycles->{$servtag}{$altnick}{report};
	    }
	    if (@{$cycles->{$servtag}{$altnick}{channels}{$channel} || []}) {
		push(@cycleall, @{$cycles->{$servtag}{$altnick}{channels}{$channel}});
	    }
	}
    }

    my $period = Irssi::settings_get_int('cyclops_period') * 60;
    @cycleall = grep {$_ > $time - $period} @cycleall;
    my ($cycleall, $cyclenew) = (scalar(@cycleall)) x 2;
    if ($report != $initial) {
	$cyclenew = scalar(grep {$_ > $report} @cycleall);
    }

    my $threshold = Irssi::settings_get_int('cyclops_threshold');
    unless (($threshold && $cyclenew >= $threshold)
	    && ! ($report != $initial && $report > $time - 20)) {
	if ($type eq 'nick') {
	    $cycles->{$servtag}{$newnick} = delete($cycles->{$servtag}{$nick});
	}
	return;
    }

    my @chanign = split(',', Irssi::settings_get_str('cyclops_ignores'));
    if (@chanign && join(', ', @chanuser) ne 'general') {
	my @chancare = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @chanign} @chanuser;
	unless (@chancare && join(', ', @chancare) ne 'general') {
	    if ($type eq 'nick') {
		$cycles->{$servtag}{$newnick} = delete($cycles->{$servtag}{$nick});
	    }
	    return;
	}
    }

    $cycles->{$servtag}{$nick}{report} = $time;
    foreach my $altnick (@altnicks) {
	$cycles->{$servtag}{$altnick}{report} = $time;
    }

    if (@{$cycles->{$servtag}{$nick}{altnicks} || []}) {
	push(@altnicks, @{$cycles->{$servtag}{$nick}{altnicks}});
    }

    foreach my $altserv ($servtag =~ m/^(....)/ && grep {$_ =~ m/^\Q$1\E/i && $_ ne $servtag} keys(%{$cycles})) {
	foreach my $altnick (grep {$_ =~ m/^\Q$nickm\E[[:punct:]0-9]*$/i
		|| $nickm =~ m/^\Q$_\E[[:punct:]0-9]*$/i} keys(%{$cycles->{$altserv}})) {
	    if (! $verify || ($verify eq 'user' && grep {$_ =~ m/^\~?([^@]+)/; grep {/^\~?\Q$1\E/i}
			@{$cycles->{$altserv}{$altnick}{addresses}}} @{$cycles->{$servtag}{$nick}{addresses}})
		    || ($verify eq 'full' && grep {my $addr = $_; grep {/^\Q$addr\E$/i}
			@{$cycles->{$altserv}{$altnick}{addresses}}} @{$cycles->{$servtag}{$nick}{addresses}})) {
		if ($altnick eq $nick) {
		    $cycles->{$altserv}{$altnick}{report} = $time;
		}
		unless (grep {$_ eq $altnick} @altnicks) {
		    push(@altnicks, $altnick);
		}
		if (@{$cycles->{$altserv}{$altnick}{altnicks} || []} && (my @altns = grep {my $altn = $_;
			! grep {$_ eq $altn} @altnicks} @{$cycles->{$altserv}{$altnick}{altnicks}})) {
		    push(@altnicks, @altns);
		}
		if (my @chans = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @chanuser}
			keys(%{$cycles->{$altserv}{$altnick}{channels}})) {
		    push(@chanuser, @chans);
		}
	    }
	}
    }

    if ($type eq 'nick') {
	$cycles->{$servtag}{$newnick} = delete($cycles->{$servtag}{$nick});
	$nick = $newnick;
    }

    my ($altnicks, $chanrest) = ('', '');
    if (@altnicks = grep {$_ ne $nick} @altnicks) {
	my $altncnt = scalar(@altnicks);
	if ($altncnt <= 5) {
	    $altnicks = ", " . join(', ', @altnicks);
	} else {
	    $altnicks = ", " . join(', ', @altnicks[0..4]) . ", and @{[$altncnt - 5]} more";
	}
    }

    my @chanrest = grep {$_ !~ m/^\Q$channel\E$/i && $_ ne 'general'} @chanuser;
    if (@chanrest) {
	if ($channel eq 'general') {
	    $channel = join(', ', @chanrest);
	} else {
	    $chanrest = ", " . join(', ', @chanrest);
	}
    }

    my ($periodall, $periodnew, $cyperall) = ($time - $initial, $time - $report, "initial");
    if ($periodall > $period) {
	$periodall = $period;
	$cyperall = "expired";
	if ($periodnew > $period) {
	    $periodnew = $period;
	}
    }

    if ($periodnew != $periodall) {
	$periodall = format_time($periodall);
	$cyperall = "$cycleall in $periodall";
    }
    $periodnew = format_time($periodnew);

    my ($opchan, $opserv) = split('/', Irssi::settings_get_str('cyclops_target'), 2);
    if ($opchan && (($opserv && ($opserv = Irssi::server_find_tag($opserv))
	    && ($opchan = $opserv->window_item_find($opchan)))
	    || ($opchan = Irssi::window_item_find($opchan)))) {
	$opchan->command("notice * Excessive \x02cycling\x02 from \x02$nick\x02$altnicks [$address] :: "
	    . "\x02$cyclenew\x02 events in \x02$periodnew\x02 ($cyperall) :: \x02$channel\x02$chanrest :: $server->{real_address}");
	$opchan->window()->activity(1);
    } else {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'cyclops_notify', $nick, $altnicks, $address,
	    $cyclenew, $periodnew, $cyperall, $channel, $chanrest);
    }
}

sub garbage {
    # Run the garbage collection every interval.
    my ($time, $period) = (time(), Irssi::settings_get_int('cyclops_period') * 60);

    foreach my $servtag (keys(%{$cycles})) {
	foreach my $nick (keys(%{$cycles->{$servtag}})) {
	    foreach my $channel (keys(%{$cycles->{$servtag}{$nick}{channels}})) {
		@{$cycles->{$servtag}{$nick}{channels}{$channel}} = grep {$_ > $time - $period}
		    @{$cycles->{$servtag}{$nick}{channels}{$channel}};
		unless (@{$cycles->{$servtag}{$nick}{channels}{$channel} || []}) {
		    delete($cycles->{$servtag}{$nick}{channels}{$channel});
		}
	    }
	    unless (keys(%{$cycles->{$servtag}{$nick}{channels}})) {
		delete($cycles->{$servtag}{$nick});
	    }
	}
	unless (keys(%{$cycles->{$servtag}})) {
	    delete($cycles->{$servtag});
	}
    }
}

sub print_chans {
    my ($data, $server, $witem) = @_;
    my ($nick) = split(' ', $data);
    my @channels;

    foreach my $servtag (keys(%{$cycles})) {
	foreach my $nickm (grep {/^\Q$nick\E$/i} keys(%{$cycles->{$servtag}})) {
	    if (my @chans = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @channels}
		    keys(%{$cycles->{$servtag}{$nickm}{channels}})) {
		push(@channels, @chans);
	    }
	}
    }

    if (@channels && join(', ', @channels) ne 'general') {
	@channels = grep {$_ ne 'general'} @channels;
    }

    if ($witem) {
	$witem->printformat(MSGLEVEL_CLIENTCRAP, 'cyclops_channels', $nick, join(' ', @channels));
    } else {
	Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'cyclops_channels', $nick, join(' ', @channels));
    }
}

sub format_time {
    my ($time) = (@_);
    if ($time > 3600) {
	return sprintf('%d:%02d:%02d', $time / 3600, ($time % 3600) / 60, $time % 60);
    } else {
	return sprintf('%d:%02d', $time / 60, $time % 60);
    }
}

# JOIN received.
sub cyclops_join {
    my ($server, $data, $nick, $address) = @_;
    my ($channel) = split(' ', $data);
    $channel =~ s/^://;
    Irssi::signal_continue(@_);
    cyclops($server, $channel, $nick, '', $address, 'join');
}

# PART received.
sub cyclops_part {
    my ($server, $data, $nick, $address) = @_;
    my ($channel) = split(' ', $data);
    $channel =~ s/^://;
    Irssi::signal_continue(@_);
    cyclops($server, $channel, $nick, '', $address, 'part');
}

# QUIT received.
sub cyclops_quit {
    my ($server, $data, $nick, $address) = @_;
    Irssi::signal_continue(@_);
    cyclops($server, 'general', $nick, '', $address, 'quit');
}

# NICK change received.
sub cyclops_nick {
    my ($server, $data, $nick, $address) = @_;
    my ($newnick) = split(' ', $data);
    $newnick =~ s/^://;
    Irssi::signal_continue(@_);
    cyclops($server, 'general', $nick, $newnick, $address, 'nick');
}

# KICK received.
sub cyclops_kick {
    my ($server, $data) = @_;
    my ($address, ($channel, $nick)) = ('', split(' ', $data));
    unless ($address = $server->window_item_find($channel)->nick_find($nick)->{host}) {
	$address = "kick::" . $channel;
    }
    Irssi::signal_continue(@_);
    cyclops($server, $channel, $nick, '', $address, 'kick');
}

Irssi::theme_register([
    'cyclops_notify' => 'Excessive {hilight cycling} from {nick $0}$1 {nickhost $2} :: '
			. '{hilight $3} events in {hilight $4} {parens $5} :: {channel $6}$7',
    'cyclops_channels' => '$0: {channel $1}'
]);

Irssi::settings_add_int('cyclops', 'cyclops_period', 180);
Irssi::settings_add_int('cyclops', 'cyclops_garbage', 90);
Irssi::settings_add_int('cyclops', 'cyclops_threshold', 6);
Irssi::settings_add_str('cyclops', 'cyclops_verify', 'user');
Irssi::settings_add_str('cyclops', 'cyclops_ignores', '');
Irssi::settings_add_str('cyclops', 'cyclops_target', '');

Irssi::timeout_add(Irssi::settings_get_int('cyclops_garbage') * 60000, 'garbage', '');

Irssi::command_bind('cyclops', 'print_chans');

Irssi::signal_add_first({
    'event join' => 'cyclops_join',
    'event part' => 'cyclops_part',
    'event quit' => 'cyclops_quit',
    'event nick' => 'cyclops_nick',
    'event kick' => 'cyclops_kick'
});
