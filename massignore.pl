use strict;
use warnings;
use Irssi;

our $VERSION = '0.5';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Mass Ignore',
    description => 'Ignores messages that include many nicks, or are repeated.',
    license     => 'GNU GPL v3 or later'
);

# delay     = period in minutes
# garbage   = period in minutes
# cache     = period in minutes, or zero to disable
# timeout   = period in minutes, or zero to never
# nick_threshold = nick count, or zero to disable
# message_threshold = message count, or zero to disable
# channels  = comma-separated list, or 'all'
# whitelist = comma-separated list of regular expressions
# prefix    = regular expression of message
#             prefix, e.g. from a bouncer

my $massmsg = {};
my $massnicks = {};

sub massignore {
    my ($server, $msg, $nick, $address, $target) = @_;
    my @channels = split(',', Irssi::settings_get_str('massignore_channels'));
    my @whitelist = split(',', Irssi::settings_get_str('massignore_whitelist'));

    if (! ((@channels && ($channels[0] =~ m/^all$/i || grep {/^\Q$target\E$/i} @channels))
	    || $target =~ m/^\Q$server->{nick}\E$/i) || (@whitelist
		&& grep {$nick =~ m/$_/i || $address =~ m/$_/i} @whitelist)) {
	return;
    }

    my ($servtag, $channel) = ($server->{tag});
    if ($target =~ m/^[#&]/) {
	$channel = $server->channel_find($target);
    }

    if (my $prefix = Irssi::settings_get_str('massignore_prefix')) {
	$msg =~ s/^$prefix//i;
    }

    if (length($msg) > 400) {
	$msg = substr($msg, 0, 400);
	if ($msg =~ m/^(.+?)\s+(\S*)$/
		&& length($2) / length($msg) <= 0.05) {
	    $msg = $1;
	}
    }

    # Disregard surrounding colors and bells
    $msg =~ s/^(?:\x03(?:[0-9]{1,2}(?:,[0-9]{1,2})?\S*)?|\a)*\s*(.+?)\s*(\x03([0-9]{1,2}(,[0-9]{1,2})?\S*)?|\a)*$/$1/;

    my $nickthrs = Irssi::settings_get_int('massignore_nick_threshold');
    my $cache = Irssi::settings_get_int('massignore_cache') * 60;
    my $timeout = Irssi::settings_get_int('massignore_timeout') * 60;
    my ($time, @msgbits, @nicks, $nickspam) = (time(), split(/\s+/, $msg));
    if ($channel) {
	if ($nickthrs && scalar(@msgbits) >= $nickthrs) {
	    if ($cache && (my @nickch = grep {$_ > $time - $cache} sort {$b <=> $a}
		    keys(%{$massnicks->{$servtag}{$target}}))) {
		@nicks = @{$massnicks->{$servtag}{$target}{$nickch[0]}};
	    } else {
		foreach my $nick ($channel->nicks()) {
		    push(@nicks, $nick->{nick});
		}
		if ($cache) {
		    @{$massnicks->{$servtag}{$target}{$time}} = @nicks;
		}
	    }
	    my @msgnicks = grep {my $nick = $_; grep {/^\Q$nick\E$/} @msgbits} @nicks;
	    if (scalar(@msgnicks) >= $nickthrs) {
		$nickspam = 1;
		Irssi::signal_stop();
		@msgbits = grep {my $msgbit = $_; ! grep {/^\Q$msgbit\E$/} @msgnicks} @msgbits;
		$msg = get_pattern_core($msg, @msgbits);
	    }
	} elsif (length($msg) <= 30) {
	    return;
	}
    }

    my $msgthrs = Irssi::settings_get_int('massignore_message_threshold');
    my $delay = Irssi::settings_get_int('massignore_delay') * 60;
    unless ($msgthrs) {
	return;
    }

    my ($mtchcnt, $failcnt) = (0, 0);
    my @msgall = @msgbits; my @msgrest = @msgbits;
    OUTER: foreach my $msgtime (grep {$_ > $time - $delay} sort {$b <=> $a}
		keys(%{$massmsg->{$servtag}{$target}})) {
	foreach my $message (reverse(@{$massmsg->{$servtag}{$target}{$msgtime}})) {
	    my @msgprev = split(/\s+/, $message);
	    my $msglgth = length($message) / length($msg);
	    my @msgmtch = grep {my $msgbit = $_; grep {/^\Q$msgbit\E$/} @msgprev} @msgbits;
	    if (@msgbits && ($msglgth >= 0.75 && $msglgth <= 1.25)
		    && length(join('', @msgmtch)) / length(join('', @msgbits)) >= 0.33) {
		push(@msgall, @msgprev);
		@msgrest = grep {my $msgbit = $_; grep {/^\Q$msgbit\E$/} @msgmtch} @msgrest;
		if (++$mtchcnt >= $msgthrs && length(join('', @msgrest))
			/ (length(join('', @msgall)) / ($mtchcnt + 1)) >= 0.33) {
		    Irssi::signal_stop() unless $nickspam;
		    set_ignore($server, $channel, $target, '', $timeout, $msg, @msgrest);
		    last OUTER;
		}
	    } elsif (! @msgbits && ! @msgprev && ! $nickspam) {
		if (++$mtchcnt >= $msgthrs) {
		    Irssi::signal_stop();
		    set_ignore($server, $channel, $target, '^\\s+$', $timeout);
		    last OUTER;
		}
	    } elsif (++$failcnt >= $msgthrs) {
		last OUTER;
	    }
	}
    }

    push(@{$massmsg->{$servtag}{$target}{$time}}, $msg);
}

sub garbage {
    # Run the garbage collection every interval.
    my $delay = Irssi::settings_get_int('massignore_delay') * 60;
    my $cache = Irssi::settings_get_int('massignore_cache') * 60;
    my $time = time();

    foreach my $servtag (keys(%{$massmsg})) {
	foreach my $target (keys(%{$massmsg->{$servtag}})) {
	    foreach my $msgtime (grep {$_ <= $time - $delay}
		    keys(%{$massmsg->{$servtag}{$target}})) {
		delete($massmsg->{$servtag}{$target}{$msgtime});
	    }
	    unless (keys(%{$massmsg->{$servtag}{$target}})) {
		delete($massmsg->{$servtag}{$target});
	    }
	}
	unless (keys(%{$massmsg->{$servtag}})) {
	    delete($massmsg->{$servtag});
	}
    }

    foreach my $servtag (keys(%{$massnicks})) {
	foreach my $channel (keys(%{$massnicks->{$servtag}})) {
	    foreach my $nicktime (grep {$_ <= $time - $cache}
		    keys(%{$massnicks->{$servtag}{$channel}})) {
		delete($massnicks->{$servtag}{$channel}{$nicktime});
	    }
	    unless (keys(%{$massnicks->{$servtag}{$channel}})) {
		delete($massnicks->{$servtag}{$channel});
	    }
	}
	unless (keys(%{$massnicks->{$servtag}})) {
	    delete($massnicks->{$servtag});
	}
    }
}

sub get_pattern_core {
    my ($msg, @msgrest) = @_;
    # Work around some funky character limit.
    my $msgpatt = substr(join(' ', @msgrest), 0, 280);
    $msgpatt =~ s/[])}\\^\$.*+?|{([]/\\$&/g;
    $msgpatt =~ s/ /\\s+(\\S+\\s+)*/g;
    $msg =~ m/$msgpatt/;
    return $&;
}

sub get_pattern_regex {
    my ($msg, @msgrest) = @_;
    my $msgpatt = get_pattern_core($msg, @msgrest) =~ s/[])}\\^\$.*+?|{([]/\\$&/gr;
    foreach my $msgbit (grep {my $msgbit = $_;
	    ! grep {/^$msgbit$/} @msgrest} split(/\s+/, $msgpatt)) {
	$msgpatt =~ s/(\s)\Q$msgbit\E(\s)/$1\\S+$2/;
    }
    $msgpatt =~ s/(\s+\\S\+){2,}.*//;
    return $msgpatt =~ s/\s+/\\s+/gr;
}

sub set_ignore {
    my ($server, $channel, $target, $msgpatt, $timeout, $msg, @msgrest) = @_;
    unless ($msgpatt) {
	if ($msg && @msgrest) {
	    $msgpatt = get_pattern_regex($msg, @msgrest);
	    unless ($msgpatt && length((($msgpatt =~ s/\\S\+\s+//gr)
		    =~ s/\\s\+/ /gr) =~ s/\\([^\\])/$1/gr) > 10) {
		return;
	    }
	} else {
	    return;
	}
    }
    $msgpatt =~ s/\\/\\\\/g;
    $msgpatt =~ s/"/\\"/g;
    if ($channel) {
	$server->command("ignore -regexp -pattern \"$msgpatt\" -channels $target"
	    . " -time $timeout *!*@* PUBLICS NOTICES ACTIONS");
    } else {
	$server->command("ignore -regexp -pattern \"$msgpatt\""
	    . " -time $timeout *!*@* MSGS NOTICES ACTIONS");
    }
}

# Channel message received.
sub massignore_message {
    my ($server, $msg, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    massignore($server, $msg, $nick, $address, $target);
}

# OP-directed PUBLIC received.
# Currently aren't checked against ignores by Irssi itself.
sub massignore_op_public {
    my ($server, $msg, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    unless ($server->ignore_check($nick, $address, $target, $msg, MSGLEVEL_PUBLIC)) {
	massignore($server, $msg, $nick, $address, $target);
    } else {
	Irssi::signal_stop();
    }
}

# NOTICE received.
# Getting through to here even if ignored.
sub massignore_notice {
    my ($server, $msg, $nick, $address, $target) = @_;
    $address = '' unless $address;
    $target =~ s/^[@+]([#&])/$1/;
    unless ($server->ignore_check($nick, $address, $target, $msg, MSGLEVEL_NOTICES)) {
	massignore($server, $msg, $nick, $address, $target);
    }
}

# ACTION received.
# Getting through to here even if ignored.
sub massignore_action {
    my ($server, $msg, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    unless ($server->ignore_check($nick, $address, $target, $msg, MSGLEVEL_ACTIONS)) {
	massignore($server, $msg, $nick, $address, $target);
    }
}

Irssi::settings_add_int('massignore', 'massignore_cache', 5);
Irssi::settings_add_int('massignore', 'massignore_delay', 10);
Irssi::settings_add_int('massignore', 'massignore_timeout', 60);
Irssi::settings_add_int('massignore', 'massignore_garbage', 60);
Irssi::settings_add_int('massignore', 'massignore_nick_threshold', 10);
Irssi::settings_add_int('massignore', 'massignore_message_threshold', 3);
Irssi::settings_add_str('massignore', 'massignore_channels', '');
Irssi::settings_add_str('massignore', 'massignore_whitelist', '');
Irssi::settings_add_str('massignore', 'massignore_prefix', '');

Irssi::timeout_add(Irssi::settings_get_int('massignore_garbage') * 60000, 'garbage', '');

Irssi::signal_add({
    'message public' => 'massignore_message',
    'message irc op_public' => 'massignore_op_public',
    'message private' => 'massignore_message',
    'message irc notice' => 'massignore_notice',
    'message irc action' => 'massignore_action'
});
