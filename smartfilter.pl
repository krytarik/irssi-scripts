use strict;
use warnings;
use Irssi;

our $VERSION = '1.8';
our %IRSSI = (
    authors     => 'Christian Brassat, Niall Bunting, Walter Hop, Krytarik Raido',
    contact     => 'irssi-smartfilter@spam.lifeforms.nl, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Smart Filter',
    description => 'Improved smart filter for join, part, quit, nick messages',
    license     => 'BSD',
    changed     => '2019-09-30'
);

# Do checks after receiving a channel event.
# - If the originating nick is not active, ignore the signal.
# - If nick is active, propagate the signal and display the event message.
#   Keep the nick marked as active, so we will not miss a re-join after a PART
#   or QUIT, a second nick change, etc.
#
# enabled    = boolean
# delay      = period in minutes
# garbage    = period in minutes
# automode   = period in seconds
# threshold  = user count
# channels   = comma-separated list of channels, or 'all'
# whitelist  = comma-separated list of regular expressions
#
# autoset    - set channels based on user count
# toggle     - toggle filter for active channel
# toggle all - toggle filter globally

my $lastmsg = {};
my $lastjoin = {};

sub smartfilter {
    my ($server, $channel, $nick, $newnick, $address, $type) = @_;

    if ($nick eq $server->{nick}) {
	return 0;
    }

    # Fuzzy network matching
    my $servtag = lc(substr($server->{tag}, 0, 4));
    my $time = time();

    my @channels = split(',', Irssi::settings_get_str('smartfilter_channels'));
    my @whitelist = split(',', Irssi::settings_get_str('smartfilter_whitelist'));
    my $enabled = Irssi::settings_get_bool('smartfilter_enabled');
    my $delay = Irssi::settings_get_int('smartfilter_delay') * 60;
    my $automode = Irssi::settings_get_int('smartfilter_automode');

    # Needed for auto-voice/-op filtering
    if ($type eq 'join' && ! ($lastmsg->{$servtag}{$nick} && $lastmsg->{$servtag}{$nick}{time}
	    && $lastmsg->{$servtag}{$nick}{time} > $time - $delay)) {
	$lastjoin->{$servtag}{$nick} = $time;
    }

    if (! $enabled || ! @channels
	    || ($channel && $channels[0] !~ m/^all$/i && ! grep {/^\Q$channel\E$/i} @channels)
	    || (@whitelist && grep {$nick =~ m/$_/i || $newnick =~ m/$_/i
		|| $address =~ m/$_/i} @whitelist)) {
	$lastmsg->{$servtag}{$nick}{time} = $time;
	if ($type =~ m/^(msgact|mode|join)$/) {
	    unless (@{$lastmsg->{$servtag}{$nick}{channels} || []}
		    && grep {/^\Q$channel\E$/i} @{$lastmsg->{$servtag}{$nick}{channels}}) {
		push(@{$lastmsg->{$servtag}{$nick}{channels}}, $channel);
	    }
	} elsif ($type eq 'status') {
	    unless (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
		push(@{$lastmsg->{$servtag}{$nick}{channels}}, 'general');
	    }
	} elsif ($type eq 'nick') {
	    $lastmsg->{$servtag}{$newnick} = $lastmsg->{$servtag}{$nick};
	    if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
		@{$lastmsg->{$servtag}{$nick}{channels}} = ();
	    } else {
		push(@{$lastmsg->{$servtag}{$newnick}{channels}}, 'general');
	    }
	} elsif ($type =~ m/^(part|kick)$/) {
	    if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
		@{$lastmsg->{$servtag}{$nick}{channels}} = grep {!/^\Q$channel\E$/i}
		    @{$lastmsg->{$servtag}{$nick}{channels}};
		if (join(', ', @{$lastmsg->{$servtag}{$nick}{channels}}) eq 'general') {
		    @{$lastmsg->{$servtag}{$nick}{channels}} = ();
		}
	    }
	} elsif ($type eq 'quit') {
	    if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
		@{$lastmsg->{$servtag}{$nick}{channels}} = ();
	    }
	}
	return 0;
    }

    unless ($type =~ m/^(msgact|kick)$/ || ($type eq 'mode'
	    && (! $lastjoin->{$servtag}{$nick} || $lastjoin->{$servtag}{$nick} <= $time - $automode))
	    || ($lastmsg->{$servtag}{$nick} && (@{$lastmsg->{$servtag}{$nick}{channels} || []}
	    || ($lastmsg->{$servtag}{$nick}{time} && $lastmsg->{$servtag}{$nick}{time} > $time - $delay)))
	    || ($type eq 'nick' && $lastmsg->{$servtag}{$newnick} && (@{$lastmsg->{$servtag}{$newnick}{channels} || []}
	    || ($lastmsg->{$servtag}{$newnick}{time} && $lastmsg->{$servtag}{$newnick}{time} > $time - $delay)))) {
	Irssi::signal_stop();
	return 1;
    }

    $lastmsg->{$servtag}{$nick}{time} = $time;
    if ($type eq 'nick') {
	$lastmsg->{$servtag}{$newnick} = $lastmsg->{$servtag}{$nick};
	if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
	    @{$lastmsg->{$servtag}{$nick}{channels}} = ();
	}
    } elsif ($type =~ m/^(part|kick)$/) {
	if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
	    @{$lastmsg->{$servtag}{$nick}{channels}} = grep {!/^\Q$channel\E$/i}
		@{$lastmsg->{$servtag}{$nick}{channels}};
	    if (join(', ', @{$lastmsg->{$servtag}{$nick}{channels}}) eq 'general') {
		@{$lastmsg->{$servtag}{$nick}{channels}} = ();
	    }
	}
    } elsif ($type eq 'quit') {
	if (@{$lastmsg->{$servtag}{$nick}{channels} || []}) {
	    @{$lastmsg->{$servtag}{$nick}{channels}} = ();
	}
    }
    return 0;
}

sub garbage {
    # Run the garbage collection every interval.
    my $time = time();
    my $delay = Irssi::settings_get_int('smartfilter_delay') * 60;
    my $automode = Irssi::settings_get_int('smartfilter_automode');

    foreach my $servtag (keys(%{$lastmsg})) {
	foreach my $nick (keys(%{$lastmsg->{$servtag}})) {
	    if (! @{$lastmsg->{$servtag}{$nick}{channels} || []}
		    && $lastmsg->{$servtag}{$nick}{time} <= $time - $delay) {
		delete($lastmsg->{$servtag}{$nick});
	    }
	}
	unless (keys(%{$lastmsg->{$servtag}})) {
	    delete($lastmsg->{$servtag});
	}
    }

    foreach my $servtag (keys(%{$lastjoin})) {
	foreach my $nick (keys(%{$lastjoin->{$servtag}})) {
	    if ($lastjoin->{$servtag}{$nick} <= $time - $automode) {
		delete($lastjoin->{$servtag}{$nick});
	    }
	}
	unless (keys(%{$lastjoin->{$servtag}})) {
	    delete($lastjoin->{$servtag});
	}
    }
}

sub autoset {
    my ($data, $server, $witem) = @_;
    my @channels_curr = split(',', Irssi::settings_get_str('smartfilter_channels'));
    my (@channels, @channels_add, @channels_drop, @output);

    foreach my $channel (Irssi::channels()) {
	if (scalar(@{[$channel->nicks()]}) >= 200) {
	    $channel = $channel->{name};
	    push(@channels, $channel);
	    unless (grep {/^\Q$channel\E$/i} @channels_curr) {
		push(@channels_add, $channel);
	    }
	}
    }

    foreach my $channel (@channels_curr) {
	unless (grep {/^\Q$channel\E$/i} @channels) {
	    push(@channels_drop, $channel);
	}
    }

    if (@channels_add || @channels_drop) {
	Irssi::settings_set_str('smartfilter_channels', join(',', @channels));
	Irssi::signal_emit('setup changed');
	if (@channels_add) {
	    push(@output, 'Smart Filter ON: ' . join(', ', @channels_add));
	}
	if (@channels_drop) {
	    push(@output, 'Smart Filter OFF: ' . join(', ', @channels_drop));
	}
	push(@output, 'Smart Filter NEW: ' . join(', ', @channels));
    } else {
	push(@output, 'Smart Filter UNCH: ' . join(', ', @channels_curr));
    }

    if ($witem) {
	$witem->print(join("\n", @output), MSGLEVEL_CLIENTCRAP);
    } else {
	Irssi::print(join("\n", @output), MSGLEVEL_CLIENTCRAP);
    }
}

sub toggle {
    my ($data, $server, $witem) = @_;
    my @args = split(' ', $data);

    unless (@args && $args[0] eq 'all') {
	if ($witem && $witem->{type} eq 'CHANNEL') {
	    my $channel = $witem->{name};
	    my @channels = split(',', Irssi::settings_get_str('smartfilter_channels'));
	    my $newstate;

	    if (grep {/^\Q$channel\E$/i} @channels) {
		@channels = grep {!/^\Q$channel\E$/i} @channels;
		$newstate = 'OFF';
	    } else {
		push(@channels, $channel);
		$newstate = 'ON';
	    }

	    Irssi::settings_set_str('smartfilter_channels', join(',', @channels));
	    Irssi::signal_emit('setup changed');
	    $witem->print('Smart Filter ' . $newstate . ': ' . $channel, MSGLEVEL_CLIENTCRAP);
	}
    } else {
	Irssi::command_runsub('smartfilter toggle', $data, $server, $witem);
    }
}

sub toggle_all {
    my ($data, $server, $witem) = @_;
    my $newstate = ! Irssi::settings_get_bool('smartfilter_enabled');
    Irssi::settings_set_bool('smartfilter_enabled', $newstate);
    Irssi::signal_emit('setup changed');

    if ($newstate) {
	$newstate = 'ON';
    } else {
	$newstate = 'OFF';
    }

    if ($witem) {
	$witem->print('Smart Filter ' . $newstate, MSGLEVEL_CLIENTCRAP);
    } else {
	Irssi::print('Smart Filter ' . $newstate, MSGLEVEL_CLIENTCRAP);
    }
}

# JOIN received.
sub smartfilter_join {
    my ($server, $channel, $nick, $address) = @_;
    smartfilter($server, $channel, $nick, '', $address, 'join');
}

# PART received.
sub smartfilter_part {
    my ($server, $channel, $nick, $address, $reason) = @_;
    unless ($reason =~ m/^requested by [^ ]+ \(.+\)$/) {
	smartfilter($server, $channel, $nick, '', $address, 'part');
    } else {
	smartfilter($server, $channel, $nick, '', $address, 'kick');
    }
}

# QUIT received.
sub smartfilter_quit {
    my ($server, $nick, $address) = @_;
    smartfilter($server, '', $nick, '', $address, 'quit');
}

# NICK change received.
sub smartfilter_nick {
    my ($server, $newnick, $nick, $address) = @_;
    smartfilter($server, '', $nick, $newnick, $address, 'nick');
}

# KICK received.
sub smartfilter_kick {
    my ($server, $channel, $nick) = @_;
    smartfilter($server, $channel, $nick, '', '', 'kick');
}

# MODE change received.
sub smartfilter_mode {
    my ($server, $channel, $nick, $address, $mode) = @_;
    # Filter auto-voices and -ops
    if ($mode =~ m/^\+[vo] ([^ ]+)$/) {
	smartfilter($server, $channel, $1, '', '', 'mode');
    }
}

# Status change received.
sub smartfilter_status {
    my ($server, $nick, $address, $data) = @_;
    smartfilter($server, '', $nick, '', $address, 'status');
}

# Channel message received.
sub smartfilter_msgact {
    my ($server, $msg, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    smartfilter($server, $target, $nick, '', $address, 'msgact');
}

Irssi::signal_add({
    'message public' => 'smartfilter_msgact',
    'message irc op_public' => 'smartfilter_msgact',
    'message private' => 'smartfilter_msgact',
    'message irc action' => 'smartfilter_msgact',
    'message irc notice' => 'smartfilter_msgact',
    'message join' => 'smartfilter_join',
    'message part' => 'smartfilter_part',
    'message quit' => 'smartfilter_quit',
    'message nick' => 'smartfilter_nick',
    'message kick' => 'smartfilter_kick',
    'message irc mode' => 'smartfilter_mode',
    'message account_changed' => 'smartfilter_status',
    'message host_changed' => 'smartfilter_status',
    'message away_notify' => 'smartfilter_status'
});

Irssi::settings_add_bool('smartfilter', 'smartfilter_enabled', 1);
Irssi::settings_add_int('smartfilter', 'smartfilter_delay', 20);
Irssi::settings_add_int('smartfilter', 'smartfilter_garbage', 60);
Irssi::settings_add_int('smartfilter', 'smartfilter_automode', 30);
Irssi::settings_add_int('smartfilter', 'smartfilter_threshold', 200);
Irssi::settings_add_str('smartfilter', 'smartfilter_channels', '');
Irssi::settings_add_str('smartfilter', 'smartfilter_whitelist', '');

Irssi::timeout_add(Irssi::settings_get_int('smartfilter_garbage') * 60000, 'garbage', '');

Irssi::command_bind({
    'smartfilter' => sub {
	my ($data, $server, $witem) = @_;
	Irssi::command_runsub('smartfilter', $data, $server, $witem);
    },
    'smartfilter autoset' => 'autoset',
    'smartfilter toggle' => 'toggle',
    'smartfilter toggle all' => 'toggle_all'
});
