use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Randomize',
    description => 'Randomizes various types of messages, or input.',
    license     => 'GNU GPL v3 or later'
);

sub randomize {
    my (@messages) = @_;
    return $messages[int(rand(scalar(@messages)))];
}

sub get_message {
    my ($type) = @_;
    my @messages = split('\|', Irssi::settings_get_str('randomize_' . $type));

    return "" unless @messages;
    return randomize(@messages);
}

sub quit {
    Irssi::command("quit " . get_message('quits'));
}

sub disconnect {
    my ($data, $server) = @_;
    my @args = split(' ', $data);

    unless (@args) {
	return unless $server;
	@args = ($server->{tag});
    }
    Irssi::command("disconnect ". $args[0] ." ". get_message('quits'));
}

sub part {
    my ($data, $server, $witem) = @_;
    my @args = split(' ', $data);

    return unless $server;
    unless (@args) {
	return unless $witem;
	@args = ($witem->{name});
    }
    $server->command("part ". $args[0] ." ". get_message('quits'));
}

sub input {
    my ($data, $server, $witem) = @_;
    my @args = split('\|', $data);

    return unless @args;
    if ($witem) {
	$witem->print('Result: ' . randomize(@args), MSGLEVEL_CLIENTCRAP);
    } else {
	Irssi::print('Result: ' . randomize(@args), MSGLEVEL_CLIENTCRAP);
    }
}

Irssi::settings_add_str('randomize', 'randomize_quits',
    "Verlaat|Abandonando|Gedirəm|Выходжу|Напускам|Me'n vaig|Odcházím|Smutter|" .
    "Verlassend|Αποχώρησε|Leaving|Saliendo|Lahkun|Irteten|Lähdössä|Quitte|" .
    "Abandonando|છોડી રહ્યા છીએ|छोड़ रहा है|Távozom|Pergi|Sto andando via|さようなら|" .
    "ಹೊರಹೋಗಲಾಗುತ್ತಿದೆ|전 이만 갑니다|Išeinu|Atstāj|Си одам|പോകുന്നു |Forlater kanalen|" .
    "Ik ga weg|ਹਟ ਪਿੱਛੇ ਫੇਰ ਮਿਲਾਗੇਂ|Wychodzi|Saindo|Ухожу я от вас|Odchádzam|Odhajam|" .
    "Po dilet|Одлазим|Lämnar|กำลังออก|Ayrılıyor|Залишаю|Tôi đi|Cwitant|已离开|暫離"
);

Irssi::command_bind({
    rquit => 'quit',
    rdisconnect => 'disconnect',
    rpart => 'part',
    random => 'input'
}, 'randomize');
