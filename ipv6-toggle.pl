use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'IPv6 Toggle',
    description => "Toggles the 'resolve_prefer_ipv6' setting "
                   . "to work around its current forcing effect.",
    license     => 'GNU GPL v3 or later'
);

my $ipv6_toggled = 0;

sub ipv6_toggle {
    my $ipv6 = Irssi::settings_get_bool('resolve_prefer_ipv6');
    Irssi::settings_set_bool('resolve_prefer_ipv6', !$ipv6);
    Irssi::signal_emit('setup changed');
    Irssi::printformat(MSGLEVEL_CLIENTNOTICE, 'ipv6_toggle', !$ipv6 ? "6" : "4");
}

sub connection_failed {
    my $time = time() / 60 * 1000;
    unless ((Irssi::settings_get_bool('ipv6_toggle_ipv4safe')
	    && ! Irssi::settings_get_bool('resolve_prefer_ipv6'))
	    || $ipv6_toggled > $time - Irssi::settings_get_time('server_reconnect_time')) {
	$ipv6_toggled = $time;
	Irssi::signal_continue(@_);
	ipv6_toggle();
    }
}

Irssi::theme_register([
    'ipv6_toggle' => 'Changed to {hilight IPv$0} resolution'
]);

Irssi::settings_add_bool('ipv6_toggle', 'ipv6_toggle_ipv4safe', 1);
Irssi::command_bind('ipv6_toggle', 'ipv6_toggle');
Irssi::signal_add('server connect failed', 'connection_failed');
