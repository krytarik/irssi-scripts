use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Address',
    description => 'Addresses someone with a given text, or not.',
    license     => 'GNU GPL v3 or later'
);

sub address {
    my ($args, $server, $witem) = @_;

    if (! $witem) {
	return;
    }

    $args =~ /^\s*(.*?)\s*:+\s*(.*?)\s*$/;
    my ($nicks, $text) = ($1, $2);

    if (! $text) {
	return;
    }

    if ($nicks) {
	$nicks =~ s/[\s,]+/, /g;
	$nicks .= ": ";
    }

    $witem->command("msg * " . $nicks . $text);
}

Irssi::command_bind('address', 'address');
