use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Event Stop',
    description => 'Stops arbitrary events.',
    license     => 'GNU GPL v3 or later'
);

my $events;

sub event_stop {
    Irssi::signal_stop()
}

sub setup_changed {
    my $events_old = $events;
    $events = Irssi::settings_get_str('event_stop_numbers');
    return if (defined $events_old && $events eq $events_old);

    if ($events_old) {
	foreach my $event (split(' ', $events_old)) {
	    Irssi::signal_remove('event ' . $event, 'event_stop');
	}
    }

    if ($events) {
	foreach my $event (split(' ', $events)) {
	    Irssi::signal_add_first('event ' . $event, 'event_stop');
	}
    }
}

Irssi::settings_add_str('event_stop', 'event_stop_numbers', '');
setup_changed();

Irssi::signal_add('setup changed', 'setup_changed');
