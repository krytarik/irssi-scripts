#
# This Irssi script splits overlong messages into multiple ones,
# without cutting off words, as well as adding ellipses.
#

use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Split Long Messages',
    description => 'Splits messages longer than 510 characters in a nice way.',
    license     => 'GNU GPL v3 or later'
);

sub splitlongmsg {
    my ($args, $server, $witem) = @_;
    my ($param, $target, $msgorig) = $args =~ /^(-[^ ]+[ ]+")?([^ "]*)"?[ ]*(.*)$/;
    my $msgmax = 444 - length($target);
    if (length($msgorig) > $msgmax) {
	my $msgrest = $msgorig;
	my ($msgcut, $msgsplit, $splitnr);
	while ($msgrest) {
	    $splitnr++;
	    if ($splitnr == 1 or length($msgrest) + 4 <= $msgmax) {
		$msgcut = $msgmax - 4;
	    } else {
		$msgcut = $msgmax - 8;
	    }
	    $msgsplit = substr($msgrest, 0, $msgcut);
	    if (length($msgrest) > $msgcut) {
		$msgsplit =~ s/[ ]+[^ ]+$//;
	    }
	    $msgrest = substr($msgrest, length($msgsplit));
	    $msgrest =~ s/^[ ]+//;
	    if ($splitnr == 1) {
		$msgsplit = $msgsplit." ...";
	    } elsif ($msgrest) {
		$msgsplit = "... ".$msgsplit." ...";
	    } else {
		$msgsplit = "... ".$msgsplit;
	    }
	    Irssi::signal_emit("command msg", $target." ".$msgsplit, $server, $witem);
	}
	Irssi::signal_stop();
    }
}

Irssi::command_bind('msg', 'splitlongmsg');
