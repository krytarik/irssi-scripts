use strict;
use warnings;
use Irssi;

our $VERSION = '0.2';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'OP List',
    description => 'Lists the OPs and Voices in a channel.',
    license     => 'GNU GPL v3 or later'
);

sub oplist {
    my ($args, $server, $witem, $nops) = @_;

    if (! $witem || $witem->{type} ne 'CHANNEL') {
	return;
    }

    my $ops = {};
    my $output;
    my $channel = $witem->{name};
    my ($opno, $halfopno, $voiceno, $normalno) = (0, 0, 0, 0);

    foreach my $nick ($witem->nicks()) {
	my $op = $nick->{nick};
	my ($rank, $prefix) = (0);

	if ($nick->{op}) {
	    $rank += 4;
	    $prefix .= '@';
	}
	if ($nick->{halfop}) {
	    $rank += 2;
	    $prefix .= '%%';
	}
	if ($nick->{voice}) {
	    $rank += 1;
	    $prefix .= '+';
	}

	if ($rank > 0) {
	    if (! $nops) {
		$ops->{$rank}{$op} = $prefix;
	    }
	} elsif ($nops) {
	    # non-breaking space
	    $ops->{$rank}{$op} = ' ';
	}
    }

    if (! $nops) {
	$output = '{names_users Operators {names_channel '.$channel.'}}%:';
    } else {
	$output = '{names_users Non-operators {names_channel '.$channel.'}}%:';
    }

    if (keys(%{$ops})) {
	$output .= '%|';
	foreach my $rank (sort {$b <=> $a} keys(%{$ops})) {
	    foreach my $op (sort {lc($a) cmp lc($b)} keys(%{$ops->{$rank}})) {
		if ($rank >= 4) {
		    $opno++;
		    $output .= '{names_nick_op '.$ops->{$rank}{$op}.' '.$op.'}';
		}
		elsif ($rank >= 2) {
		    $halfopno++;
		    $output .= '{names_nick_halfop '.$ops->{$rank}{$op}.' '.$op.'}';
		}
		elsif ($rank >= 1) {
		    $voiceno++;
		    $output .= '{names_nick_voice '.$ops->{$rank}{$op}.' '.$op.'}';
		}
		else {
		    $normalno++;
		    $output .= '{names_nick '.$ops->{$rank}{$op}.' '.$op.'}';
		}
	    }
	}
	$output .= '%:'
    }

    if (! $nops) {
	my $opallno = $opno + $halfopno + $voiceno;
	$output .= sprintf('{line_start}{hilight Irssi:} {channel %s}: Total of {hilight %s} specials '
		       . '[{hilight %s} ops, {hilight %s} halfops, {hilight %s} voices]',
		       $channel, $opallno, $opno, $halfopno, $voiceno);
    } else {
	$output .= sprintf('{line_start}{hilight Irssi:} {channel %s}: Total of {hilight %s} normals',
		       $channel, $normalno);
    }

    $witem->print(Irssi::current_theme()->format_expand($output), MSGLEVEL_CLIENTCRAP);
}

sub noplist {
    my ($args, $server, $witem) = @_;
    oplist($args, $server, $witem, 'nops');
}

Irssi::command_bind('ops', 'oplist');
Irssi::command_bind('nops', 'noplist');
