use strict;
use warnings;
use Irssi;

our $VERSION = '1.04';
our %IRSSI = (
    authors     => 'ilbelkyr, krytarik',
    contact     => 'ilbelkyr@shalture.org, krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'away-notify',
    description => 'Display away status changes using CAP away-notify',
    license     => 'CC0 1.0 <https://creativecommons.org/publicdomain/zero/1.0/legalcode>',
    changed     => 'Thu Aug 22 03:56:04 CEST 2019',
);

my $cap_toggle = Irssi::Irc::Server->can('irc_server_cap_toggle');

sub on_away {
    my ( $server, $away, $nick, $address ) = @_;
    $away =~ s/^://;
    my @refnums;

    # Print the notification to all shared channels
    foreach my $channel ( $server->channels() ) {
	if ( $channel->nick_find($nick)
		&& ! check_ignore( $server, $nick, $address, $channel->{name} ) ) {
	    my $refnum = $channel->window()->{refnum};
	    unless ( grep { $_ == $refnum } @refnums ) {
		push( @refnums, $refnum );
		print_away( $channel, $nick, $away );
	    }
	}
    }

    # ...and the nick's query if any
    if ( ( my $query = $server->query_find($nick) )
	    && ! check_ignore( $server, $nick, $address, '' ) ) {
	my $refnum = $query->window()->{refnum};
	unless ( grep { $_ == $refnum } @refnums ) {
	    push( @refnums, $refnum );
	    print_away( $query, $nick, $away );
	}
    }
}

sub check_ignore {
    my ( $server, $nick, $address, $channel ) = @_;

    if ( $server->ignore_check( $nick, $address, $channel, '', MSGLEVEL_JOINS ) ) {
	return 1;
    }

    if ( my $smartfilter = Irssi::Script::smartfilter->can('smartfilter') ) {
	if ( $smartfilter->( $server, $channel, $nick, '', $address, 'status' ) ) {
	    return 1;
	}
    }
}

sub print_away {
    my ( $witem, $nick, $away ) = @_;

    if ( $away ) {
	$witem->printformat( MSGLEVEL_JOINS, 'away_notify_gone', $nick, $away );
    } else {
	$witem->printformat( MSGLEVEL_JOINS, 'away_notify_back', $nick );
    }
}

sub on_cap {
    my ( $server, $data ) = @_;

    if ( $data =~ m/^\S+ NAK :away-notify\s*$/ ) {
	Irssi::signal_stop();
    }
}

sub on_connected {
    my ( $server ) = @_;

    if ( $cap_toggle ) {
	$server->irc_server_cap_toggle( 'away-notify', 1 );
    } else {
	$server->command( 'quote cap req :away-notify' );
    }
}

Irssi::theme_register([
    away_notify_gone => '{channick $0} is now away {reason $1}',
    away_notify_back => '{channick_hilight $0} is back',
]);

Irssi::signal_add_first( 'event cap', 'on_cap' ) unless $cap_toggle;

Irssi::signal_add({
    'event away'   => 'on_away',
    'event connected' => 'on_connected',
});

foreach my $server ( Irssi::servers() ) {
    on_connected($server);
}
