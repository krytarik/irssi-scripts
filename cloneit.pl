use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'CloneIT',
    description => 'Detects and notifies about excessive clones.',
    license     => 'GNU GPL v3 or later'
);

# garbage   = period in minutes
# threshold = number of clones, or zero to disable
# ignores   = comma-separated list of channels
# target    = report to '<channel>[/<server>]',
#             or empty to just notify

my $clones = {};

sub cloneit {
    my ($server, $channel, $nick, $newnick, $address, $type) = @_;

    # Fuzzy network matching
    my $servtag = lc(substr($server->{tag}, 0, 4));
    $address =~ s/^.*@//;

    unless ($type eq 'join') {
	if ($clones->{$servtag}{$address}) {
	    if ($type =~ m/^(part|kick)$/) {
		if (@{$clones->{$servtag}{$address}{channels}{$channel} || []}) {
		    @{$clones->{$servtag}{$address}{channels}{$channel}} = grep {$_ ne $nick}
			@{$clones->{$servtag}{$address}{channels}{$channel}};
		}
	    } elsif ($type eq 'quit') {
		foreach my $channel (keys(%{$clones->{$servtag}{$address}{channels}})) {
		    @{$clones->{$servtag}{$address}{channels}{$channel}} = grep {$_ ne $nick}
			@{$clones->{$servtag}{$address}{channels}{$channel}};
		}
	    } elsif ($type eq 'nick') {
		foreach my $channel (keys(%{$clones->{$servtag}{$address}{channels}})) {
		    @{$clones->{$servtag}{$address}{channels}{$channel}} = grep {$_ ne $nick}
			@{$clones->{$servtag}{$address}{channels}{$channel}};
		    push(@{$clones->{$servtag}{$address}{channels}{$channel}}, $newnick);
		}
	    }
	}
	return;
    }

    unless (@{$clones->{$servtag}{$address}{channels}{$channel} || []}
	    && grep {$_ eq $nick} @{$clones->{$servtag}{$address}{channels}{$channel}}) {
	push(@{$clones->{$servtag}{$address}{channels}{$channel}}, $nick);
    }

    my $threshold = Irssi::settings_get_int('cloneit_threshold');
    my $clonecnt = scalar(@{$clones->{$servtag}{$address}{channels}{$channel}});
    unless ($threshold && ! ($clonecnt % $threshold)) {
	return;
    }

    my $time = time();
    if ($clones->{$servtag}{$nick}{report}
	    && $clones->{$servtag}{$nick}{report} > $time - 20) {
	return;
    }
    $clones->{$servtag}{$nick}{report} = $time;

    my @chanuser = keys(%{$clones->{$servtag}{$address}{channels}});
    my @chanign = split(',', Irssi::settings_get_str('cloneit_ignores'));
    if (@chanign) {
	my @chancare = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @chanign} @chanuser;
	unless (@chancare) {
	    return;
	}
    }

    my ($nicks, $chanrest) = ('', '');
    if (scalar(@{$clones->{$servtag}{$address}{channels}{$channel}}) <= 6) {
	$nicks = join(', ', @{$clones->{$servtag}{$address}{channels}{$channel}});
    } else {
	$nicks = join(', ', @{$clones->{$servtag}{$address}{channels}{$channel}}[0..5]) . ", ...";
    }

    my @chanrest = grep {$_ !~ m/^\Q$channel\E$/i} @chanuser;
    if (@chanrest) {
	$chanrest = ", " . join(', ', @chanrest);
    }

    my ($opchan, $opserv) = split('/', Irssi::settings_get_str('cloneit_target'), 2);
    if ($opchan && (($opserv && ($opserv = Irssi::server_find_tag($opserv))
	    && ($opchan = $opserv->window_item_find($opchan)))
	    || ($opchan = Irssi::window_item_find($opchan)))) {
	$opchan->command("notice * Excessive \x02clones\x02 from \x02*!*\@$address\x02 :: $nicks :: "
	    . "\x02$clonecnt\x02 clones :: \x02$channel\x02$chanrest :: $server->{real_address}");
	$opchan->window()->activity(1);
    } else {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'cloneit_notify', $address, $nicks,
	    $clonecnt, $channel, $chanrest);
    }
}

sub garbage {
    # Run the garbage collection every interval.
    foreach my $servtag (keys(%{$clones})) {
	foreach my $address (keys(%{$clones->{$servtag}})) {
	    foreach my $channel (keys(%{$clones->{$servtag}{$address}{channels}})) {
		unless (@{$clones->{$servtag}{$address}{channels}{$channel} || []}) {
		    delete($clones->{$servtag}{$address}{channels}{$channel});
		}
	    }
	    unless (keys(%{$clones->{$servtag}{$address}{channels}})) {
		delete($clones->{$servtag}{$address});
	    }
	}
	unless (keys(%{$clones->{$servtag}})) {
	    delete($clones->{$servtag});
	}
    }
}

sub print_chans {
    my ($data, $server, $witem) = @_;
    my ($address) = split(' ', $data);
    $address =~ s/^.*@//;
    my @channels;

    foreach my $servtag (keys(%{$clones})) {
	foreach my $addressm (grep {/^\Q$address\E$/i} keys(%{$clones->{$servtag}})) {
	    if (my @chans = grep {my $chan = $_; ! grep {/^\Q$chan\E$/i} @channels}
		    keys(%{$clones->{$servtag}{$addressm}{channels}})) {
		push(@channels, @chans);
	    }
	}
    }

    if ($witem) {
	$witem->printformat(MSGLEVEL_CLIENTCRAP, 'cloneit_channels', $address, join(' ', @channels));
    } else {
	Irssi::printformat(MSGLEVEL_CLIENTCRAP, 'cloneit_channels', $address, join(' ', @channels));
    }
}

# JOIN received.
sub cloneit_join {
    my ($server, $data, $nick, $address) = @_;
    my ($channel) = split(' ', $data);
    $channel =~ s/^://;
    Irssi::signal_continue(@_);
    cloneit($server, $channel, $nick, '', $address, 'join');
}

# PART received.
sub cloneit_part {
    my ($server, $data, $nick, $address) = @_;
    my ($channel) = split(' ', $data);
    $channel =~ s/^://;
    Irssi::signal_continue(@_);
    cloneit($server, $channel, $nick, '', $address, 'part');
}

# QUIT received.
sub cloneit_quit {
    my ($server, $data, $nick, $address) = @_;
    Irssi::signal_continue(@_);
    cloneit($server, '', $nick, '', $address, 'quit');
}

# NICK change received.
sub cloneit_nick {
    my ($server, $data, $nick, $address) = @_;
    my ($newnick) = split(' ', $data);
    $newnick =~ s/^://;
    Irssi::signal_continue(@_);
    cloneit($server, '', $nick, $newnick, $address, 'nick');
}

# KICK received.
sub cloneit_kick {
    my ($server, $data) = @_;
    my ($channel, $nick) = split(' ', $data);
    my $address = $server->window_item_find($channel)->nick_find($nick)->{host};
    Irssi::signal_continue(@_);
    if ($address) {
	cloneit($server, $channel, $nick, '', $address, 'kick');
    }
}

Irssi::theme_register([
    'cloneit_notify' => 'Excessive {hilight clones} from {nick *!*@$0} :: $1 :: '
			. '{hilight $2} clones :: {channel $3}$4',
    'cloneit_channels' => '*!*@$0: {channel $1}'
]);

Irssi::settings_add_int('cloneit', 'cloneit_garbage', 60);
Irssi::settings_add_int('cloneit', 'cloneit_threshold', 4);
Irssi::settings_add_str('cloneit', 'cloneit_ignores', '');
Irssi::settings_add_str('cloneit', 'cloneit_target', '');

Irssi::timeout_add(Irssi::settings_get_int('cloneit_garbage') * 60000, 'garbage', '');

Irssi::command_bind('cloneit', 'print_chans');

Irssi::signal_add_first({
    'event join' => 'cloneit_join',
    'event part' => 'cloneit_part',
    'event quit' => 'cloneit_quit',
    'event nick' => 'cloneit_nick',
    'event kick' => 'cloneit_kick'
});
