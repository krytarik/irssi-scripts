use strict;
use warnings;
use Irssi;

our $VERSION = '0.3';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'Ignotify',
    description => 'Notifies about events that are ignored.',
    license     => 'GNU GPL v3 or later'
);

# level  = message levels to notify about
# target = report to '<channel>[/<server>]',
#          or empty to just notify

my $level;

sub ctcp {
    my ($server, $data, $nick, $address, $target) = @_;
    $data =~ m/^\s*((DCC\s+)?\S+)?\s*(.*?)\s*$/;
    my ($type, $text) = ($1, $3);
    $target =~ s/^[@+]([#&])/$1/;

    if ($type eq 'ACTION') {
	return unless $level & MSGLEVEL_ACTIONS and $server->ignore_check($nick, $address, $target, $text, MSGLEVEL_ACTIONS);
    } elsif ($type =~ m/^DCC\s+/) {
	return unless $level & MSGLEVEL_DCC and $server->ignore_check($nick, $address, $target, $text, MSGLEVEL_DCC);
    } else {
	return unless $level & MSGLEVEL_CTCPS and $server->ignore_check($nick, $address, $target, $text, MSGLEVEL_CTCPS);
	$type = "CTCP " . $type;
    }

    message_notify($type, $server, $nick, $address, $target, $text);
}

sub public {
    my ($server, $data, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    if ($server->ignore_check($nick, $address, $target, $data, MSGLEVEL_PUBLIC)) {
	message_notify('PUBLIC', $server, $nick, $address, $target, $data);
    }
}

sub private {
    my ($server, $data, $nick, $address, $target) = @_;
    $target =~ s/^[@+]([#&])/$1/;
    if ($server->ignore_check($nick, $address, $target, $data, MSGLEVEL_MSGS)) {
	message_notify('PRIVATE', $server, $nick, $address, $target, $data);
    }
}

sub notice {
    my ($server, $data, $nick, $address, $target) = @_;
    $address = '' unless $address;
    $target =~ s/^[@+]([#&])/$1/;
    if ($server->ignore_check($nick, $address, $target, $data, MSGLEVEL_NOTICES)) {
	message_notify('NOTICE', $server, $nick, $address, $target, $data);
    }
}

sub join {
    my ($server, $target, $nick, $address) = @_;
    if ($server->ignore_check($nick, $address, $target, '', MSGLEVEL_JOINS)) {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_join', $nick, $address, $target);
    }
}

sub part {
    my ($server, $target, $nick, $address, $data) = @_;
    if ($server->ignore_check($nick, $address, $target, $data, MSGLEVEL_PARTS)) {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_part', $nick, $address, $target, $data);
    }
}

sub quit {
    my ($server, $nick, $address, $data) = @_;
    if ($server->ignore_check($nick, $address, '', $data, MSGLEVEL_QUITS)) {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_quit', $nick, $address, $data);
    }
}

sub nick {
    my ($server, $nick_new, $nick_old, $address) = @_;
    if ($server->ignore_check($nick_old, $address, '', '', MSGLEVEL_NICKS)
	    || $server->ignore_check($nick_new, $address, '', '', MSGLEVEL_NICKS)) {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_nick', $nick_old, $address, $nick_new);
    }
}

sub kick {
    my ($server, $target, $nick, $kicker, $address, $data) = @_;
    if ($server->ignore_check($kicker, $address, $target, $data, MSGLEVEL_KICKS)) {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_kick', $nick, $target, $kicker, $address, $data);
    }
}

sub message_notify {
    my ($type, $server, $nick, $address, $target, $data) = @_;
    my ($opchan, $opserv) = split('/', Irssi::settings_get_str('ignotify_target'), 2);
    if ($opchan && (($opserv && ($opserv = Irssi::server_find_tag($opserv))
	    && ($opchan = $opserv->window_item_find($opchan)))
	    || ($opchan = Irssi::window_item_find($opchan)))) {
	$opchan->command("notice * Ignored \x02$type\x02 from \x02$nick\x02 [$address] to \x02$target\x02: $data"
	    . " :: $server->{real_address}");
	$opchan->window()->activity(1);
    } else {
	$server->printformat('', MSGLEVEL_CLIENTCRAP, 'ignotify_message', $type, $nick, $address, $target, $data);
    }
}

sub setup_changed {
    my $level_old = $level;
    $level = Irssi::settings_get_level('ignotify_level');
    return if (defined $level_old && $level == $level_old);

    if ($level_old) {
	Irssi::signal_remove('ctcp msg', 'ctcp');
	Irssi::signal_remove('message public', 'public');
	Irssi::signal_remove('message irc op_public', 'public');
	Irssi::signal_remove('message private', 'private');
	Irssi::signal_remove('message irc notice', 'notice');
	Irssi::signal_remove('message join', 'join');
	Irssi::signal_remove('message part', 'part');
	Irssi::signal_remove('message quit', 'quit');
	Irssi::signal_remove('message nick', 'nick');
	Irssi::signal_remove('message kick', 'kick');
    }

    if ($level) {
	Irssi::signal_add_first('ctcp msg', 'ctcp')
	    if $level & (MSGLEVEL_CTCPS|MSGLEVEL_DCC|MSGLEVEL_ACTIONS);

	Irssi::signal_add_first({'message public' => 'public',
	    'message irc op_public' => 'public'}) if $level & MSGLEVEL_PUBLIC;
	Irssi::signal_add_first('message private', 'private') if $level & MSGLEVEL_MSGS;
	Irssi::signal_add_first('message irc notice', 'notice') if $level & MSGLEVEL_NOTICES;

	Irssi::signal_add_first('message join', 'join') if $level & MSGLEVEL_JOINS;
	Irssi::signal_add_first('message part', 'part') if $level & MSGLEVEL_PARTS;
	Irssi::signal_add_first('message quit', 'quit') if $level & MSGLEVEL_QUITS;
	Irssi::signal_add_first('message nick', 'nick') if $level & MSGLEVEL_NICKS;
	Irssi::signal_add_first('message kick', 'kick') if $level & MSGLEVEL_KICKS;
    }
}

Irssi::theme_register([
    ignotify_message => 'Ignored {hilight $0} from {nick $1} {nickhost $2} to {nick $3}: $4',
    ignotify_join    => 'Ignored {hilight JOIN} from {nick $0} {nickhost $1} on {channel $2}',
    ignotify_part    => 'Ignored {hilight PART} from {nick $0} {nickhost $1} on {channel $2} {reason $3}',
    ignotify_quit    => 'Ignored {hilight QUIT} from {nick $0} {nickhost $1} {reason $2}',
    ignotify_nick    => 'Ignored {hilight NICK} from {nick $0} {nickhost $1} to {nick $2}',
    ignotify_kick    => 'Ignored {hilight KICK} of {nick $0} on {channel $1} by {nick $2} {nickhost $3} {reason $4}'
]);

Irssi::settings_add_level('ignotify', 'ignotify_level', 'CTCPS DCC');
Irssi::settings_add_str('ignotify', 'ignotify_target', '');
setup_changed();

Irssi::signal_add('setup changed', 'setup_changed');
