# Support extended-joins in Irssi
# (c) 2012 Mike Quin <mike@elite.uk.com>
#
# Licensed under GNU General Public License version 2
#   <https://www.gnu.org/licenses/gpl-2.0.html>

use strict;
use warnings;
use Irssi;

our $VERSION = '1.0.0';
our %IRSSI = (
    authors	=> 'Mike Quin, Krytarik Raido',
    contact	=> 'mike@elite.uk.com, krytarik@tuxgarage.com',
    url		=> 'https://bitbucket.org/krytarik/irssi-scripts',
    name	=> 'cap-extjoin',
    description	=> 'Print account and realname information on joins where extended-join is available',
    license	=> 'GPLv2',
    changed	=> 'Tue Feb 13 21:00:00 CET 2018'
);

my $cap_toggle = Irssi::Irc::Server->can('irc_server_cap_toggle');

sub event_join {
    my ($server, $data, $nick, $address) = @_;

    my ($channel, $account, $realname);
    unless ($data =~ /^(\S+) (\S+) :(.+)$/
	    and ! $server->netsplit_find($nick, $address)) {
	return;
    } else {
	($channel, $account, $realname) = ($1, $2, $3);
    }

    Irssi::signal_stop();

    if ($server->ignore_check($nick, $address, $channel, '', MSGLEVEL_JOINS)) {
	return;
    }

    if (my $smartfilter = Irssi::Script::smartfilter->can('smartfilter')) {
	if ($smartfilter->($server, $channel, $nick, '', $address, 'join')) {
	    return;
	}
    }

    my $chanrec = $server->channel_find($channel);
    $chanrec->printformat(MSGLEVEL_JOINS, 'join_extended',
	$nick, $account, $realname, $address, $channel);
}

sub extjoin_cap_reply {
    my ($server, $data) = @_;
    if ($data =~ /^\S+ NAK :extended-join\s*$/) {
	Irssi::signal_stop();
    }
}

sub extjoin_connected {
    my ($server) = @_;
    if ($cap_toggle) {
	$server->irc_server_cap_toggle('extended-join', 1);
    } else {
	$server->command("quote cap req :extended-join");
    }
}

Irssi::theme_register([
    'join_extended' => '{channick_hilight $0} {chanhost_hilight $3} [a: $1, r: $2] has joined {channel $4}'
]);

Irssi::signal_add_first('event cap', 'extjoin_cap_reply') unless $cap_toggle;

Irssi::signal_add({
    'event join' => 'event_join',
    'event connected' => 'extjoin_connected'
});

# On load enumerate the servers and try to turn on extended-join
foreach my $server (Irssi::servers()) {
    extjoin_connected($server);
}
