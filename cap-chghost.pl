use strict;
use warnings;
use Irssi;

our $VERSION = '1.01';
our %IRSSI = (
    authors     => 'krytarik',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'cap-chghost',
    description => 'Display host and user name changes using CAP chghost',
    license     => 'CC0 1.0 <https://creativecommons.org/publicdomain/zero/1.0/legalcode>',
    changed     => 'Thu Aug 22 03:56:04 CEST 2019',
);

my $cap_toggle = Irssi::Irc::Server->can('irc_server_cap_toggle');

sub on_chghost {
    my ( $server, $chghost, $nick, $address ) = @_;
    my ( $curuser, $curhost ) = split( '@', $address );
    $chghost =~ s/^://;
    my ( $newuser, $newhost ) = split( ' ', $chghost );
    my $newaddr = $newuser . '@' . $newhost;
    my @refnums;

    # Print the notification to all shared channels
    foreach my $channel ( $server->channels() ) {
	if ( $channel->nick_find($nick)
		&& ! check_ignore( $server, $nick, $address, $newaddr, $channel->{name} ) ) {
	    my $refnum = $channel->window()->{refnum};
	    unless ( grep { $_ == $refnum } @refnums ) {
		push( @refnums, $refnum );
		print_chghost( $channel, $nick, $curuser, $curhost, $newuser, $newhost );
	    }
	}
    }

    # ...and the nick's query if any
    if ( ( my $query = $server->query_find($nick) )
	    && ! check_ignore( $server, $nick, $address, $newaddr, '' ) ) {
	my $refnum = $query->window()->{refnum};
	unless ( grep { $_ == $refnum } @refnums ) {
	    push( @refnums, $refnum );
	    print_chghost( $query, $nick, $curuser, $curhost, $newuser, $newhost );
	}
    }
}

sub check_ignore {
    my ( $server, $nick, $address, $newaddr, $channel ) = @_;

    if ( $server->ignore_check( $nick, $address, $channel, '', MSGLEVEL_JOINS )
	    || $server->ignore_check( $nick, $newaddr, $channel, '', MSGLEVEL_JOINS ) ) {
	return 1;
    }

    if ( my $smartfilter = Irssi::Script::smartfilter->can('smartfilter') ) {
	if ( $smartfilter->( $server, $channel, $nick, '', $address, 'status' )
		|| $smartfilter->( $server, $channel, $nick, '', $newaddr, 'status' ) ) {
	    return 1;
	}
    }
}

sub print_chghost {
    my ( $witem, $nick, $curuser, $curhost, $newuser, $newhost ) = @_;

    if ( $newhost ne $curhost && $newuser eq $curuser ) {
	$witem->printformat( MSGLEVEL_JOINS, 'chghost_host', $nick, $newhost );
    } elsif ($newuser ne $curuser && $newhost eq $curhost ) {
	$witem->printformat( MSGLEVEL_JOINS, 'chghost_user', $nick, $newuser );
    } elsif ($newhost ne $curhost && $newuser ne $curuser ) {
	$witem->printformat( MSGLEVEL_JOINS, 'chghost_host_user', $nick, $newuser, $newhost );
    }
}

sub on_cap {
    my ( $server, $data ) = @_;

    if ( $data =~ m/^\S+ NAK :chghost\s*$/ ) {
	Irssi::signal_stop();
    }
}

sub on_connected {
    my ( $server ) = @_;

    if ( $cap_toggle ) {
	$server->irc_server_cap_toggle( 'chghost', 1 );
    } else {
	$server->command( 'quote cap req :chghost' );
    }
}

Irssi::theme_register([
    chghost_host => '{channick_hilight $0} has now host {hilight $1}',
    chghost_user => '{channick_hilight $0} has now user {hilight $1}',
    chghost_host_user => '{channick_hilight $0} has now user {hilight $1} and host {hilight $2}',
]);

Irssi::signal_add_first( 'event cap', 'on_cap' ) unless $cap_toggle;

Irssi::signal_add({
    'event chghost'   => 'on_chghost',
    'event connected' => 'on_connected',
});

foreach my $server ( Irssi::servers() ) {
    on_connected($server);
}
