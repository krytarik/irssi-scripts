use strict;
use warnings;
use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    authors     => 'Krytarik Raido',
    contact     => 'krytarik@tuxgarage.com',
    url         => 'https://bitbucket.org/krytarik/irssi-scripts',
    name        => 'CAP Print',
    description => 'Prints CAP replies',
    license     => 'GNU GPL v3 or later'
);

sub event_cap {
    my ($server, $data) = @_;
    $data =~ m/^\S+ (\S+) :(.*?)\s*$/;
    my ($subcmd, $reply) = ($1, $2);
    $server->printformat('', MSGLEVEL_CLIENTCRAP, 'cap_reply', $subcmd, $reply);
}

Irssi::theme_register([
    cap_reply => '{hilight CAP $0}: $1'
]);

Irssi::signal_add({
    'event cap' => 'event_cap',
    'event 410' => 'event_cap'
});
